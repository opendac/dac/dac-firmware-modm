/*
 * Copyright (c) 2019, 2021 Niklas Hauser
 *
 * This file is part of the modm project.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
// ----------------------------------------------------------------------------

namespace modm::platform
{

constexpr Rcc::flash_latency
Rcc::computeFlashLatency(uint32_t Core_Hz, uint16_t Core_mV)
{
	constexpr uint32_t flash_latency_1800[] =
	{
		24000000,
		48000000,
		72000000,
	};
	const uint32_t *lut(flash_latency_1800);
	uint8_t lut_size(sizeof(flash_latency_1800) / sizeof(uint32_t));
	(void) Core_mV;
	// find the next highest frequency in the table
	uint8_t latency(0);
	uint32_t max_freq(0);
	while (latency < lut_size)
	{
		if (Core_Hz <= (max_freq = lut[latency]))
			break;
		latency++;
	}
	return {latency, max_freq};
}

template< uint32_t Core_Hz, uint16_t Core_mV>
uint32_t
Rcc::setFlashLatency()
{
	constexpr flash_latency fl = computeFlashLatency(Core_Hz, Core_mV);
	static_assert(Core_Hz <= fl.max_frequency, "CPU Frequency is too high for this core voltage!");

	uint32_t acr = FLASH->ACR & ~FLASH_ACR_LATENCY;
	// set flash latency
	acr |= fl.latency;
	// enable flash prefetch
	acr |= FLASH_ACR_PRFTBE;
	FLASH->ACR = acr;
	__DSB(); __ISB();
	return fl.max_frequency;
}

template< uint32_t Core_Hz >
void
Rcc::updateCoreFrequency()
{
	SystemCoreClock = Core_Hz;
	delay_fcpu_MHz = computeDelayMhz(Core_Hz);
	delay_ns_per_loop = computeDelayNsPerLoop(Core_Hz);
}

constexpr bool
rcc_check_enable(Peripheral peripheral)
{
	switch(peripheral) {
		case Peripheral::Adc1:
		case Peripheral::Adc2:
		case Peripheral::Can:
		case Peripheral::Crc:
		case Peripheral::Dma1:
		case Peripheral::I2c1:
		case Peripheral::I2c2:
		case Peripheral::Rtc:
		case Peripheral::Spi1:
		case Peripheral::Spi2:
		case Peripheral::Tim1:
		case Peripheral::Tim2:
		case Peripheral::Tim3:
		case Peripheral::Tim4:
		case Peripheral::Usart1:
		case Peripheral::Usart2:
		case Peripheral::Usart3:
		case Peripheral::Usb:
		case Peripheral::Wwdg:
			return true;
		default:
			return false;
	}
}

template< Peripheral peripheral >
void
Rcc::enable()
{
	static_assert(rcc_check_enable(peripheral),
		"Rcc::enable() doesn't know this peripheral!");

	__DSB();
	if constexpr (peripheral == Peripheral::Adc1)
		if (not Rcc::isEnabled<peripheral>()) {
			RCC->APB2ENR |= RCC_APB2ENR_ADC1EN; __DSB();
			RCC->APB2RSTR |= RCC_APB2RSTR_ADC1RST; __DSB();
			RCC->APB2RSTR &= ~RCC_APB2RSTR_ADC1RST;
		}
	if constexpr (peripheral == Peripheral::Adc2)
		if (not Rcc::isEnabled<peripheral>()) {
			RCC->APB2ENR |= RCC_APB2ENR_ADC2EN; __DSB();
			RCC->APB2RSTR |= RCC_APB2RSTR_ADC2RST; __DSB();
			RCC->APB2RSTR &= ~RCC_APB2RSTR_ADC2RST;
		}
	if constexpr (peripheral == Peripheral::Can)
		if (not Rcc::isEnabled<peripheral>()) {
			RCC->APB1ENR |= RCC_APB1ENR_CAN1EN; __DSB();
			RCC->APB1RSTR |= RCC_APB1RSTR_CAN1RST; __DSB();
			RCC->APB1RSTR &= ~RCC_APB1RSTR_CAN1RST;
		}
	if constexpr (peripheral == Peripheral::Crc)
		if (not Rcc::isEnabled<peripheral>()) {
			RCC->AHBENR |= RCC_AHBENR_CRCEN;
		}
	if constexpr (peripheral == Peripheral::Dma1)
		if (not Rcc::isEnabled<peripheral>()) {
			RCC->AHBENR |= RCC_AHBENR_DMA1EN;
		}
	if constexpr (peripheral == Peripheral::I2c1)
		if (not Rcc::isEnabled<peripheral>()) {
			RCC->APB1ENR |= RCC_APB1ENR_I2C1EN; __DSB();
			RCC->APB1RSTR |= RCC_APB1RSTR_I2C1RST; __DSB();
			RCC->APB1RSTR &= ~RCC_APB1RSTR_I2C1RST;
		}
	if constexpr (peripheral == Peripheral::I2c2)
		if (not Rcc::isEnabled<peripheral>()) {
			RCC->APB1ENR |= RCC_APB1ENR_I2C2EN; __DSB();
			RCC->APB1RSTR |= RCC_APB1RSTR_I2C2RST; __DSB();
			RCC->APB1RSTR &= ~RCC_APB1RSTR_I2C2RST;
		}
	if constexpr (peripheral == Peripheral::Rtc)
		if (not Rcc::isEnabled<peripheral>()) {
			RCC->BDCR |= RCC_BDCR_RTCEN;
		}
	if constexpr (peripheral == Peripheral::Spi1)
		if (not Rcc::isEnabled<peripheral>()) {
			RCC->APB2ENR |= RCC_APB2ENR_SPI1EN; __DSB();
			RCC->APB2RSTR |= RCC_APB2RSTR_SPI1RST; __DSB();
			RCC->APB2RSTR &= ~RCC_APB2RSTR_SPI1RST;
		}
	if constexpr (peripheral == Peripheral::Spi2)
		if (not Rcc::isEnabled<peripheral>()) {
			RCC->APB1ENR |= RCC_APB1ENR_SPI2EN; __DSB();
			RCC->APB1RSTR |= RCC_APB1RSTR_SPI2RST; __DSB();
			RCC->APB1RSTR &= ~RCC_APB1RSTR_SPI2RST;
		}
	if constexpr (peripheral == Peripheral::Tim1)
		if (not Rcc::isEnabled<peripheral>()) {
			RCC->APB2ENR |= RCC_APB2ENR_TIM1EN; __DSB();
			RCC->APB2RSTR |= RCC_APB2RSTR_TIM1RST; __DSB();
			RCC->APB2RSTR &= ~RCC_APB2RSTR_TIM1RST;
		}
	if constexpr (peripheral == Peripheral::Tim2)
		if (not Rcc::isEnabled<peripheral>()) {
			RCC->APB1ENR |= RCC_APB1ENR_TIM2EN; __DSB();
			RCC->APB1RSTR |= RCC_APB1RSTR_TIM2RST; __DSB();
			RCC->APB1RSTR &= ~RCC_APB1RSTR_TIM2RST;
		}
	if constexpr (peripheral == Peripheral::Tim3)
		if (not Rcc::isEnabled<peripheral>()) {
			RCC->APB1ENR |= RCC_APB1ENR_TIM3EN; __DSB();
			RCC->APB1RSTR |= RCC_APB1RSTR_TIM3RST; __DSB();
			RCC->APB1RSTR &= ~RCC_APB1RSTR_TIM3RST;
		}
	if constexpr (peripheral == Peripheral::Tim4)
		if (not Rcc::isEnabled<peripheral>()) {
			RCC->APB1ENR |= RCC_APB1ENR_TIM4EN; __DSB();
			RCC->APB1RSTR |= RCC_APB1RSTR_TIM4RST; __DSB();
			RCC->APB1RSTR &= ~RCC_APB1RSTR_TIM4RST;
		}
	if constexpr (peripheral == Peripheral::Usart1)
		if (not Rcc::isEnabled<peripheral>()) {
			RCC->APB2ENR |= RCC_APB2ENR_USART1EN; __DSB();
			RCC->APB2RSTR |= RCC_APB2RSTR_USART1RST; __DSB();
			RCC->APB2RSTR &= ~RCC_APB2RSTR_USART1RST;
		}
	if constexpr (peripheral == Peripheral::Usart2)
		if (not Rcc::isEnabled<peripheral>()) {
			RCC->APB1ENR |= RCC_APB1ENR_USART2EN; __DSB();
			RCC->APB1RSTR |= RCC_APB1RSTR_USART2RST; __DSB();
			RCC->APB1RSTR &= ~RCC_APB1RSTR_USART2RST;
		}
	if constexpr (peripheral == Peripheral::Usart3)
		if (not Rcc::isEnabled<peripheral>()) {
			RCC->APB1ENR |= RCC_APB1ENR_USART3EN; __DSB();
			RCC->APB1RSTR |= RCC_APB1RSTR_USART3RST; __DSB();
			RCC->APB1RSTR &= ~RCC_APB1RSTR_USART3RST;
		}
	if constexpr (peripheral == Peripheral::Usb)
		if (not Rcc::isEnabled<peripheral>()) {
			RCC->APB1ENR |= RCC_APB1ENR_USBEN; __DSB();
			RCC->APB1RSTR |= RCC_APB1RSTR_USBRST; __DSB();
			RCC->APB1RSTR &= ~RCC_APB1RSTR_USBRST;
		}
	if constexpr (peripheral == Peripheral::Wwdg)
		if (not Rcc::isEnabled<peripheral>()) {
			RCC->APB1ENR |= RCC_APB1ENR_WWDGEN; __DSB();
			RCC->APB1RSTR |= RCC_APB1RSTR_WWDGRST; __DSB();
			RCC->APB1RSTR &= ~RCC_APB1RSTR_WWDGRST;
		}
	__DSB();
}

template< Peripheral peripheral >
void
Rcc::disable()
{
	static_assert(rcc_check_enable(peripheral),
		"Rcc::disable() doesn't know this peripheral!");

	__DSB();
	if constexpr (peripheral == Peripheral::Adc1) {
		RCC->APB2ENR &= ~RCC_APB2ENR_ADC1EN;
	}
	if constexpr (peripheral == Peripheral::Adc2) {
		RCC->APB2ENR &= ~RCC_APB2ENR_ADC2EN;
	}
	if constexpr (peripheral == Peripheral::Can) {
		RCC->APB1ENR &= ~RCC_APB1ENR_CAN1EN;
	}
	if constexpr (peripheral == Peripheral::Crc) {
		RCC->AHBENR &= ~RCC_AHBENR_CRCEN;
	}
	if constexpr (peripheral == Peripheral::Dma1) {
		RCC->AHBENR &= ~RCC_AHBENR_DMA1EN;
	}
	if constexpr (peripheral == Peripheral::I2c1) {
		RCC->APB1ENR &= ~RCC_APB1ENR_I2C1EN;
	}
	if constexpr (peripheral == Peripheral::I2c2) {
		RCC->APB1ENR &= ~RCC_APB1ENR_I2C2EN;
	}
	if constexpr (peripheral == Peripheral::Rtc) {
		RCC->BDCR &= ~RCC_BDCR_RTCEN;
	}
	if constexpr (peripheral == Peripheral::Spi1) {
		RCC->APB2ENR &= ~RCC_APB2ENR_SPI1EN;
	}
	if constexpr (peripheral == Peripheral::Spi2) {
		RCC->APB1ENR &= ~RCC_APB1ENR_SPI2EN;
	}
	if constexpr (peripheral == Peripheral::Tim1) {
		RCC->APB2ENR &= ~RCC_APB2ENR_TIM1EN;
	}
	if constexpr (peripheral == Peripheral::Tim2) {
		RCC->APB1ENR &= ~RCC_APB1ENR_TIM2EN;
	}
	if constexpr (peripheral == Peripheral::Tim3) {
		RCC->APB1ENR &= ~RCC_APB1ENR_TIM3EN;
	}
	if constexpr (peripheral == Peripheral::Tim4) {
		RCC->APB1ENR &= ~RCC_APB1ENR_TIM4EN;
	}
	if constexpr (peripheral == Peripheral::Usart1) {
		RCC->APB2ENR &= ~RCC_APB2ENR_USART1EN;
	}
	if constexpr (peripheral == Peripheral::Usart2) {
		RCC->APB1ENR &= ~RCC_APB1ENR_USART2EN;
	}
	if constexpr (peripheral == Peripheral::Usart3) {
		RCC->APB1ENR &= ~RCC_APB1ENR_USART3EN;
	}
	if constexpr (peripheral == Peripheral::Usb) {
		RCC->APB1ENR &= ~RCC_APB1ENR_USBEN;
	}
	if constexpr (peripheral == Peripheral::Wwdg) {
		RCC->APB1ENR &= ~RCC_APB1ENR_WWDGEN;
	}
	__DSB();
}

template< Peripheral peripheral >
bool
Rcc::isEnabled()
{
	static_assert(rcc_check_enable(peripheral),
		"Rcc::isEnabled() doesn't know this peripheral!");

	if constexpr (peripheral == Peripheral::Adc1)
		return RCC->APB2ENR & RCC_APB2ENR_ADC1EN;
	if constexpr (peripheral == Peripheral::Adc2)
		return RCC->APB2ENR & RCC_APB2ENR_ADC2EN;
	if constexpr (peripheral == Peripheral::Can)
		return RCC->APB1ENR & RCC_APB1ENR_CAN1EN;
	if constexpr (peripheral == Peripheral::Crc)
		return RCC->AHBENR & RCC_AHBENR_CRCEN;
	if constexpr (peripheral == Peripheral::Dma1)
		return RCC->AHBENR & RCC_AHBENR_DMA1EN;
	if constexpr (peripheral == Peripheral::I2c1)
		return RCC->APB1ENR & RCC_APB1ENR_I2C1EN;
	if constexpr (peripheral == Peripheral::I2c2)
		return RCC->APB1ENR & RCC_APB1ENR_I2C2EN;
	if constexpr (peripheral == Peripheral::Rtc)
		return RCC->BDCR & RCC_BDCR_RTCEN;
	if constexpr (peripheral == Peripheral::Spi1)
		return RCC->APB2ENR & RCC_APB2ENR_SPI1EN;
	if constexpr (peripheral == Peripheral::Spi2)
		return RCC->APB1ENR & RCC_APB1ENR_SPI2EN;
	if constexpr (peripheral == Peripheral::Tim1)
		return RCC->APB2ENR & RCC_APB2ENR_TIM1EN;
	if constexpr (peripheral == Peripheral::Tim2)
		return RCC->APB1ENR & RCC_APB1ENR_TIM2EN;
	if constexpr (peripheral == Peripheral::Tim3)
		return RCC->APB1ENR & RCC_APB1ENR_TIM3EN;
	if constexpr (peripheral == Peripheral::Tim4)
		return RCC->APB1ENR & RCC_APB1ENR_TIM4EN;
	if constexpr (peripheral == Peripheral::Usart1)
		return RCC->APB2ENR & RCC_APB2ENR_USART1EN;
	if constexpr (peripheral == Peripheral::Usart2)
		return RCC->APB1ENR & RCC_APB1ENR_USART2EN;
	if constexpr (peripheral == Peripheral::Usart3)
		return RCC->APB1ENR & RCC_APB1ENR_USART3EN;
	if constexpr (peripheral == Peripheral::Usb)
		return RCC->APB1ENR & RCC_APB1ENR_USBEN;
	if constexpr (peripheral == Peripheral::Wwdg)
		return RCC->APB1ENR & RCC_APB1ENR_WWDGEN;
}

}   // namespace modm::platform