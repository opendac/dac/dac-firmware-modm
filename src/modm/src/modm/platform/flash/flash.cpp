/*
 * Copyright (c) 2020, Niklas Hauser
 *
 * This file is part of the modm project.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
// ----------------------------------------------------------------------------

#include "flash.hpp"

static constexpr uint32_t FLASH_SR_ERR = 0xfffe;

namespace modm::platform
{

bool
Flash::unlock()
{
	Flash::enable();
	if (isLocked())
	{
		FLASH->KEYR = 0x45670123;
		FLASH->KEYR = 0xCDEF89AB;
	}
	return not isLocked();
}

uint8_t
Flash::getPage(uintptr_t offset)
{
	const uint8_t index = (offset >> 10);
	return index;
}

uint32_t
Flash::getOffset(uint8_t index)
{
	return (1ul << 10) * index;
}

size_t
Flash::getSize([[maybe_unused]] uint8_t index)
{
	return (1ul	<< 10);
}

modm_ramcode uint32_t
Flash::erase(uint8_t index)
{
	FLASH->CR &= ~FLASH_CR_STRT;
	FLASH->CR |= FLASH_CR_PER;
	FLASH->AR = reinterpret_cast<std::uintptr_t>(getAddr(index));
	FLASH->CR |= FLASH_CR_STRT;
	while(isBusy()) ;
	FLASH->CR &= ~FLASH_CR_PER;
	return FLASH->SR & FLASH_SR_PGERR;
}

modm_ramcode uint32_t
Flash::program(uintptr_t addr, MaxWordType data)
{
	while(isBusy()) ;
	FLASH->CR |= FLASH_CR_PG;
	*(MaxWordType*) addr = data;
	while(isBusy()) ;

	FLASH->CR &= ~FLASH_CR_PG;
	return FLASH->SR & FLASH_SR_PGERR;
}

} // namespace modm::platform