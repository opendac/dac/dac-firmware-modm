/*
 * Copyright (c) 2017, 2021, Niklas Hauser
 * Copyright (c) 2022, Andrey Kunitsyn
 *
 * This file is part of the modm project.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
// ----------------------------------------------------------------------------

#pragma once

#include "unused.hpp"
#include "static.hpp"
#include <type_traits>

/// @cond
namespace modm::platform::detail
{

template< Gpio::Signal signal, class... Signals >
struct GpioGetSignal;
template< Gpio::Signal signal, class SignalT, class... Signals >
struct GpioGetSignal<signal, SignalT, Signals...>
{
	using Gpio = std::conditional_t<
				(SignalT::Signal == signal),
				typename modm::platform::GpioStatic<typename SignalT::Data>,
				typename GpioGetSignal<signal, Signals...>::Gpio
			>;
};
template< Gpio::Signal signal >
struct GpioGetSignal<signal>
{
	using Gpio = GpioUnused;
};

} // namespace modm::platform::detail
/// @endcond

namespace modm::platform
{

/// @ingroup modm_platform_gpio
template< Peripheral peripheral, class... Signals >
struct GpioConnector
{
	template< class GpioQuery >
	static constexpr bool Contains = (
		std::is_same_v<typename Signals::Data, typename GpioQuery::Data> or ...);

	template< class GpioQuery >
	static constexpr bool IsValid = not std::is_same_v<typename GpioQuery::Data, detail::DataUnused>;

	template< Gpio::Signal signal >
	using GetSignal = typename detail::GpioGetSignal<signal, Signals...>::Gpio;

	template< class Signal >
	static void connectSignal()
	{
		using Connection = detail::SignalConnection<Signal, peripheral>;
		using Pin = GpioStatic<typename Signal::Data>;
		if constexpr(Connection::af == -2) {
			Pin::setAnalogInput();
		}
		else {
			Pin::setAlternateFunction();
		}
	}

	static inline void connect()
	{
		(connectSignal<Signals>(), ...);
	}

	static inline void disconnect()
	{
		(GpioStatic<typename Signals::Data>::disconnect(), ...);
	}
};

} // namespace modm::platform

#include <bit>

/// @cond
namespace modm::platform
{

template< class... Signals >
struct GpioConnector<Peripheral::Spi1, Signals...>
{
	template< class GpioQuery >
	static constexpr bool Contains = (
		std::is_same_v<typename Signals::Data, typename GpioQuery::Data> or ...);

	template< class GpioQuery >
	static constexpr bool IsValid = not std::is_same_v<typename GpioQuery::Data, detail::DataUnused>;

	template< Gpio::Signal signal >
	using GetSignal = typename detail::GpioGetSignal<signal, Signals...>::Gpio;

	template< class Signal >
	static void connectSignal()
	{
		using Connection = detail::SignalConnection<Signal, Peripheral::Spi1>;
		using Pin = GpioStatic<typename Signal::Data>;
		if constexpr(Connection::af == -2) {
			Pin::setAnalogInput();
		}
		else {
			Pin::setAlternateFunction();
		}
	}

	static inline void connect()
	{
		constexpr auto lmb = [](uint32_t id) { return 31 - std::countl_zero(id); };
		static constexpr uint32_t id = (detail::SignalConnection<Signals, Peripheral::Spi1>::Groups & ... & uint32_t(-1));
		static_assert((id == uint32_t(-1)) or (lmb(id) == 0ul) or (lmb(id) == 1ul),
					  "This pin set contains conflicting remap groups!\nAvailable groups for Spi1 are:\n"
					  " Spi1 |  0   |  1   \n"
					  "------|------|------\n"
					  "  A4  | Nss  |      \n"
					  "  A5  | Sck  |      \n"
					  "  A6  | Miso |      \n"
					  "  A7  | Mosi |      \n"
					  " A15  |      | Nss  \n"
					  "  B3  |      | Sck  \n"
					  "  B4  |      | Miso \n"
					  "  B5  |      | Mosi \n"
			);
		if (id != uint32_t(-1)) {
			AFIO->MAPR = (AFIO->MAPR & ~(1ul << 0)) | (lmb(id) << 0);
		}
		(connectSignal<Signals>(), ...);
	}

	static inline void disconnect()
	{
		(GpioStatic<typename Signals::Data>::disconnect(), ...);
	}
};

template< class... Signals >
struct GpioConnector<Peripheral::I2c1, Signals...>
{
	template< class GpioQuery >
	static constexpr bool Contains = (
		std::is_same_v<typename Signals::Data, typename GpioQuery::Data> or ...);

	template< class GpioQuery >
	static constexpr bool IsValid = not std::is_same_v<typename GpioQuery::Data, detail::DataUnused>;

	template< Gpio::Signal signal >
	using GetSignal = typename detail::GpioGetSignal<signal, Signals...>::Gpio;

	template< class Signal >
	static void connectSignal()
	{
		using Connection = detail::SignalConnection<Signal, Peripheral::I2c1>;
		using Pin = GpioStatic<typename Signal::Data>;
		if constexpr(Connection::af == -2) {
			Pin::setAnalogInput();
		}
		else {
			Pin::setAlternateFunction();
		}
	}

	static inline void connect()
	{
		constexpr auto lmb = [](uint32_t id) { return 31 - std::countl_zero(id); };
		static constexpr uint32_t id = (detail::SignalConnection<Signals, Peripheral::I2c1>::Groups & ... & uint32_t(-1));
		static_assert((id == uint32_t(-1)) or (lmb(id) == 0ul) or (lmb(id) == 1ul),
					  "This pin set contains conflicting remap groups!\nAvailable groups for I2c1 are:\n"
					  " I2c1 |  0  |  1  \n"
					  "------|-----|-----\n"
					  "  B6  | Scl |     \n"
					  "  B7  | Sda |     \n"
					  "  B8  |     | Scl \n"
					  "  B9  |     | Sda \n"
			);
		if (id != uint32_t(-1)) {
			AFIO->MAPR = (AFIO->MAPR & ~(1ul << 1)) | (lmb(id) << 1);
		}
		(connectSignal<Signals>(), ...);
	}

	static inline void disconnect()
	{
		(GpioStatic<typename Signals::Data>::disconnect(), ...);
	}
};

template< class... Signals >
struct GpioConnector<Peripheral::Usart1, Signals...>
{
	template< class GpioQuery >
	static constexpr bool Contains = (
		std::is_same_v<typename Signals::Data, typename GpioQuery::Data> or ...);

	template< class GpioQuery >
	static constexpr bool IsValid = not std::is_same_v<typename GpioQuery::Data, detail::DataUnused>;

	template< Gpio::Signal signal >
	using GetSignal = typename detail::GpioGetSignal<signal, Signals...>::Gpio;

	template< class Signal >
	static void connectSignal()
	{
		using Connection = detail::SignalConnection<Signal, Peripheral::Usart1>;
		using Pin = GpioStatic<typename Signal::Data>;
		if constexpr(Connection::af == -2) {
			Pin::setAnalogInput();
		}
		else {
			Pin::setAlternateFunction();
		}
	}

	static inline void connect()
	{
		constexpr auto lmb = [](uint32_t id) { return 31 - std::countl_zero(id); };
		static constexpr uint32_t id = (detail::SignalConnection<Signals, Peripheral::Usart1>::Groups & ... & uint32_t(-1));
		static_assert((id == uint32_t(-1)) or (lmb(id) == 0ul) or (lmb(id) == 1ul),
					  "This pin set contains conflicting remap groups!\nAvailable groups for Usart1 are:\n"
					  " Usart1 | 0  | 1  \n"
					  "--------|----|----\n"
					  "   A9   | Tx |    \n"
					  "  A10   | Rx |    \n"
					  "   B6   |    | Tx \n"
					  "   B7   |    | Rx \n"
			);
		if (id != uint32_t(-1)) {
			AFIO->MAPR = (AFIO->MAPR & ~(1ul << 2)) | (lmb(id) << 2);
		}
		(connectSignal<Signals>(), ...);
	}

	static inline void disconnect()
	{
		(GpioStatic<typename Signals::Data>::disconnect(), ...);
	}
};

template< class... Signals >
struct GpioConnector<Peripheral::Usart2, Signals...>
{
	template< class GpioQuery >
	static constexpr bool Contains = (
		std::is_same_v<typename Signals::Data, typename GpioQuery::Data> or ...);

	template< class GpioQuery >
	static constexpr bool IsValid = not std::is_same_v<typename GpioQuery::Data, detail::DataUnused>;

	template< Gpio::Signal signal >
	using GetSignal = typename detail::GpioGetSignal<signal, Signals...>::Gpio;

	template< class Signal >
	static void connectSignal()
	{
		using Connection = detail::SignalConnection<Signal, Peripheral::Usart2>;
		using Pin = GpioStatic<typename Signal::Data>;
		if constexpr(Connection::af == -2) {
			Pin::setAnalogInput();
		}
		else {
			Pin::setAlternateFunction();
		}
	}

	static inline void connect()
	{
		constexpr auto lmb = [](uint32_t id) { return 31 - std::countl_zero(id); };
		static constexpr uint32_t id = (detail::SignalConnection<Signals, Peripheral::Usart2>::Groups & ... & uint32_t(-1));
		static_assert((id == uint32_t(-1)) or (lmb(id) == 0ul),
					  "This pin set contains conflicting remap groups!\nAvailable groups for Usart2 are:\n"
					  " Usart2 |  0  \n"
					  "--------|-----\n"
					  "   A0   | Cts \n"
					  "   A1   | Rts \n"
					  "   A2   |  Tx \n"
					  "   A3   |  Rx \n"
					  "   A4   |  Ck \n"
			);
		if (id != uint32_t(-1)) {
			AFIO->MAPR = (AFIO->MAPR & ~(1ul << 3)) | (lmb(id) << 3);
		}
		(connectSignal<Signals>(), ...);
	}

	static inline void disconnect()
	{
		(GpioStatic<typename Signals::Data>::disconnect(), ...);
	}
};

template< class... Signals >
struct GpioConnector<Peripheral::Usart3, Signals...>
{
	template< class GpioQuery >
	static constexpr bool Contains = (
		std::is_same_v<typename Signals::Data, typename GpioQuery::Data> or ...);

	template< class GpioQuery >
	static constexpr bool IsValid = not std::is_same_v<typename GpioQuery::Data, detail::DataUnused>;

	template< Gpio::Signal signal >
	using GetSignal = typename detail::GpioGetSignal<signal, Signals...>::Gpio;

	template< class Signal >
	static void connectSignal()
	{
		using Connection = detail::SignalConnection<Signal, Peripheral::Usart3>;
		using Pin = GpioStatic<typename Signal::Data>;
		if constexpr(Connection::af == -2) {
			Pin::setAnalogInput();
		}
		else {
			Pin::setAlternateFunction();
		}
	}

	static inline void connect()
	{
		constexpr auto lmb = [](uint32_t id) { return 31 - std::countl_zero(id); };
		static constexpr uint32_t id = (detail::SignalConnection<Signals, Peripheral::Usart3>::Groups & ... & uint32_t(-1));
		static_assert((id == uint32_t(-1)) or (lmb(id) == 0ul) or (lmb(id) == 1ul),
					  "This pin set contains conflicting remap groups!\nAvailable groups for Usart3 are:\n"
					  " Usart3 |  0  |  1  \n"
					  "--------|-----|-----\n"
					  "  B10   |  Tx |     \n"
					  "  B11   |  Rx |     \n"
					  "  B12   |  Ck |     \n"
					  "  B13   | Cts | Cts \n"
					  "  B14   | Rts | Rts \n"
			);
		if (id != uint32_t(-1)) {
			AFIO->MAPR = (AFIO->MAPR & ~(3ul << 4)) | (lmb(id) << 4);
		}
		(connectSignal<Signals>(), ...);
	}

	static inline void disconnect()
	{
		(GpioStatic<typename Signals::Data>::disconnect(), ...);
	}
};

template< class... Signals >
struct GpioConnector<Peripheral::Tim1, Signals...>
{
	template< class GpioQuery >
	static constexpr bool Contains = (
		std::is_same_v<typename Signals::Data, typename GpioQuery::Data> or ...);

	template< class GpioQuery >
	static constexpr bool IsValid = not std::is_same_v<typename GpioQuery::Data, detail::DataUnused>;

	template< Gpio::Signal signal >
	using GetSignal = typename detail::GpioGetSignal<signal, Signals...>::Gpio;

	template< class Signal >
	static void connectSignal()
	{
		using Connection = detail::SignalConnection<Signal, Peripheral::Tim1>;
		using Pin = GpioStatic<typename Signal::Data>;
		if constexpr(Connection::af == -2) {
			Pin::setAnalogInput();
		}
		else {
			Pin::setAlternateFunction();
		}
	}

	static inline void connect()
	{
		constexpr auto lmb = [](uint32_t id) { return 31 - std::countl_zero(id); };
		static constexpr uint32_t id = (detail::SignalConnection<Signals, Peripheral::Tim1>::Groups & ... & uint32_t(-1));
		static_assert((id == uint32_t(-1)) or (lmb(id) == 0ul) or (lmb(id) == 1ul),
					  "This pin set contains conflicting remap groups!\nAvailable groups for Tim1 are:\n"
					  " Tim1 |  0   |  1   \n"
					  "------|------|------\n"
					  "  A6  |      | Bkin \n"
					  "  A7  |      | Ch1n \n"
					  "  A8  | Ch1  | Ch1  \n"
					  "  A9  | Ch2  | Ch2  \n"
					  " A10  | Ch3  | Ch3  \n"
					  " A11  | Ch4  | Ch4  \n"
					  " A12  | Etr  | Etr  \n"
					  "  B0  |      | Ch2n \n"
					  "  B1  |      | Ch3n \n"
					  " B12  | Bkin |      \n"
					  " B13  | Ch1n |      \n"
					  " B14  | Ch2n |      \n"
					  " B15  | Ch3n |      \n"
			);
		if (id != uint32_t(-1)) {
			AFIO->MAPR = (AFIO->MAPR & ~(3ul << 6)) | (lmb(id) << 6);
		}
		(connectSignal<Signals>(), ...);
	}

	static inline void disconnect()
	{
		(GpioStatic<typename Signals::Data>::disconnect(), ...);
	}
};

template< class... Signals >
struct GpioConnector<Peripheral::Tim2, Signals...>
{
	template< class GpioQuery >
	static constexpr bool Contains = (
		std::is_same_v<typename Signals::Data, typename GpioQuery::Data> or ...);

	template< class GpioQuery >
	static constexpr bool IsValid = not std::is_same_v<typename GpioQuery::Data, detail::DataUnused>;

	template< Gpio::Signal signal >
	using GetSignal = typename detail::GpioGetSignal<signal, Signals...>::Gpio;

	template< class Signal >
	static void connectSignal()
	{
		using Connection = detail::SignalConnection<Signal, Peripheral::Tim2>;
		using Pin = GpioStatic<typename Signal::Data>;
		if constexpr(Connection::af == -2) {
			Pin::setAnalogInput();
		}
		else {
			Pin::setAlternateFunction();
		}
	}

	static inline void connect()
	{
		constexpr auto lmb = [](uint32_t id) { return 31 - std::countl_zero(id); };
		static constexpr uint32_t id = (detail::SignalConnection<Signals, Peripheral::Tim2>::Groups & ... & uint32_t(-1));
		static_assert((id == uint32_t(-1)) or (lmb(id) == 0ul) or (lmb(id) == 1ul) or (lmb(id) == 2ul) or (lmb(id) == 3ul),
					  "This pin set contains conflicting remap groups!\nAvailable groups for Tim2 are:\n"
					  " Tim2 |    0    |    1    |    2    |    3    \n"
					  "------|---------|---------|---------|---------\n"
					  "  A0  | Ch1,Etr |         | Ch1,Etr |         \n"
					  "  A1  |   Ch2   |         |   Ch2   |         \n"
					  "  A2  |   Ch3   |   Ch3   |         |         \n"
					  "  A3  |   Ch4   |   Ch4   |         |         \n"
					  " A15  |         | Ch1,Etr |         | Ch1,Etr \n"
					  "  B3  |         |   Ch2   |         |   Ch2   \n"
					  " B10  |         |         |   Ch3   |   Ch3   \n"
					  " B11  |         |         |   Ch4   |   Ch4   \n"
			);
		if (id != uint32_t(-1)) {
			AFIO->MAPR = (AFIO->MAPR & ~(3ul << 8)) | (lmb(id) << 8);
		}
		(connectSignal<Signals>(), ...);
	}

	static inline void disconnect()
	{
		(GpioStatic<typename Signals::Data>::disconnect(), ...);
	}
};

template< class... Signals >
struct GpioConnector<Peripheral::Tim3, Signals...>
{
	template< class GpioQuery >
	static constexpr bool Contains = (
		std::is_same_v<typename Signals::Data, typename GpioQuery::Data> or ...);

	template< class GpioQuery >
	static constexpr bool IsValid = not std::is_same_v<typename GpioQuery::Data, detail::DataUnused>;

	template< Gpio::Signal signal >
	using GetSignal = typename detail::GpioGetSignal<signal, Signals...>::Gpio;

	template< class Signal >
	static void connectSignal()
	{
		using Connection = detail::SignalConnection<Signal, Peripheral::Tim3>;
		using Pin = GpioStatic<typename Signal::Data>;
		if constexpr(Connection::af == -2) {
			Pin::setAnalogInput();
		}
		else {
			Pin::setAlternateFunction();
		}
	}

	static inline void connect()
	{
		constexpr auto lmb = [](uint32_t id) { return 31 - std::countl_zero(id); };
		static constexpr uint32_t id = (detail::SignalConnection<Signals, Peripheral::Tim3>::Groups & ... & uint32_t(-1));
		static_assert((id == uint32_t(-1)) or (lmb(id) == 0ul) or (lmb(id) == 2ul),
					  "This pin set contains conflicting remap groups!\nAvailable groups for Tim3 are:\n"
					  " Tim3 |  0  |  2  \n"
					  "------|-----|-----\n"
					  "  A6  | Ch1 |     \n"
					  "  A7  | Ch2 |     \n"
					  "  B0  | Ch3 | Ch3 \n"
					  "  B1  | Ch4 | Ch4 \n"
					  "  B4  |     | Ch1 \n"
					  "  B5  |     | Ch2 \n"
			);
		if (id != uint32_t(-1)) {
			AFIO->MAPR = (AFIO->MAPR & ~(3ul << 10)) | (lmb(id) << 10);
		}
		(connectSignal<Signals>(), ...);
	}

	static inline void disconnect()
	{
		(GpioStatic<typename Signals::Data>::disconnect(), ...);
	}
};

template< class... Signals >
struct GpioConnector<Peripheral::Tim4, Signals...>
{
	template< class GpioQuery >
	static constexpr bool Contains = (
		std::is_same_v<typename Signals::Data, typename GpioQuery::Data> or ...);

	template< class GpioQuery >
	static constexpr bool IsValid = not std::is_same_v<typename GpioQuery::Data, detail::DataUnused>;

	template< Gpio::Signal signal >
	using GetSignal = typename detail::GpioGetSignal<signal, Signals...>::Gpio;

	template< class Signal >
	static void connectSignal()
	{
		using Connection = detail::SignalConnection<Signal, Peripheral::Tim4>;
		using Pin = GpioStatic<typename Signal::Data>;
		if constexpr(Connection::af == -2) {
			Pin::setAnalogInput();
		}
		else {
			Pin::setAlternateFunction();
		}
	}

	static inline void connect()
	{
		constexpr auto lmb = [](uint32_t id) { return 31 - std::countl_zero(id); };
		static constexpr uint32_t id = (detail::SignalConnection<Signals, Peripheral::Tim4>::Groups & ... & uint32_t(-1));
		static_assert((id == uint32_t(-1)) or (lmb(id) == 0ul),
					  "This pin set contains conflicting remap groups!\nAvailable groups for Tim4 are:\n"
					  " Tim4 |  0  \n"
					  "------|-----\n"
					  "  B6  | Ch1 \n"
					  "  B7  | Ch2 \n"
					  "  B8  | Ch3 \n"
					  "  B9  | Ch4 \n"
			);
		if (id != uint32_t(-1)) {
			AFIO->MAPR = (AFIO->MAPR & ~(1ul << 12)) | (lmb(id) << 12);
		}
		(connectSignal<Signals>(), ...);
	}

	static inline void disconnect()
	{
		(GpioStatic<typename Signals::Data>::disconnect(), ...);
	}
};

template< class... Signals >
struct GpioConnector<Peripheral::Can, Signals...>
{
	template< class GpioQuery >
	static constexpr bool Contains = (
		std::is_same_v<typename Signals::Data, typename GpioQuery::Data> or ...);

	template< class GpioQuery >
	static constexpr bool IsValid = not std::is_same_v<typename GpioQuery::Data, detail::DataUnused>;

	template< Gpio::Signal signal >
	using GetSignal = typename detail::GpioGetSignal<signal, Signals...>::Gpio;

	template< class Signal >
	static void connectSignal()
	{
		using Connection = detail::SignalConnection<Signal, Peripheral::Can>;
		using Pin = GpioStatic<typename Signal::Data>;
		if constexpr(Connection::af == -2) {
			Pin::setAnalogInput();
		}
		else {
			Pin::setAlternateFunction();
		}
	}

	static inline void connect()
	{
		constexpr auto lmb = [](uint32_t id) { return 31 - std::countl_zero(id); };
		static constexpr uint32_t id = (detail::SignalConnection<Signals, Peripheral::Can>::Groups & ... & uint32_t(-1));
		static_assert((id == uint32_t(-1)) or (lmb(id) == 0ul) or (lmb(id) == 2ul) or (lmb(id) == 3ul),
					  "This pin set contains conflicting remap groups!\nAvailable groups for Can are:\n"
					  " Can | 0  | 2  | 3  \n"
					  "-----|----|----|----\n"
					  " A11 | Rx |    |    \n"
					  " A12 | Tx |    |    \n"
					  "  B8 |    | Rx |    \n"
					  "  B9 |    | Tx |    \n"
					  "  D0 |    |    | Rx \n"
					  "  D1 |    |    | Tx \n"
			);
		if (id != uint32_t(-1)) {
			AFIO->MAPR = (AFIO->MAPR & ~(3ul << 13)) | (lmb(id) << 13);
		}
		(connectSignal<Signals>(), ...);
	}

	static inline void disconnect()
	{
		(GpioStatic<typename Signals::Data>::disconnect(), ...);
	}
};
} // namespace modm::platform
/// @endcond
