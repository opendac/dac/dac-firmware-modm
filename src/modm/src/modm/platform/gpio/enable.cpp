/*
 * Copyright (c) 2013-2014, Kevin Läufer
 * Copyright (c) 2013-2018, Niklas Hauser
 *
 * This file is part of the modm project.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
// ----------------------------------------------------------------------------

#include "../device.hpp"
#include <modm/platform/core/hardware_init.hpp>

void
modm_gpio_enable(void)
{
	// Enable GPIO clock
	RCC->APB2ENR  |=
		RCC_APB2ENR_IOPAEN |
		RCC_APB2ENR_IOPBEN |
		RCC_APB2ENR_IOPCEN |
		RCC_APB2ENR_IOPDEN;
	// Reset GPIO peripheral
	RCC->APB2RSTR |=
		RCC_APB2RSTR_IOPARST |
		RCC_APB2RSTR_IOPBRST |
		RCC_APB2RSTR_IOPCRST |
		RCC_APB2RSTR_IOPDRST;
	RCC->APB2RSTR &= ~(
		RCC_APB2RSTR_IOPARST |
		RCC_APB2RSTR_IOPBRST |
		RCC_APB2RSTR_IOPCRST |
		RCC_APB2RSTR_IOPDRST);
}

MODM_HARDWARE_INIT_ORDER(modm_gpio_enable, 80);