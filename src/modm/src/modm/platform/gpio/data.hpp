/*
 * Copyright (c) 2021, Niklas Hauser
 *
 * This file is part of the modm project.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
// ----------------------------------------------------------------------------

#pragma once

#include "base.hpp"

/// @cond
namespace modm::platform::detail
{

template<class Signal, Peripheral p> struct SignalConnection;
template<class Data, Peripheral p> static constexpr int8_t AdcChannel = -1;
template<class Data, Peripheral p> static constexpr int8_t DacChannel = -1;

struct DataUnused {};

struct DataA0 {
	static constexpr Gpio::Port port = Gpio::Port::A;
	static constexpr uint8_t pin = 0;
	struct BitBang { using Data = DataA0; static constexpr Gpio::Signal Signal = Gpio::Signal::BitBang; };
	struct Ch1 { using Data = DataA0; static constexpr Gpio::Signal Signal = Gpio::Signal::Ch1; };
	struct Cts { using Data = DataA0; static constexpr Gpio::Signal Signal = Gpio::Signal::Cts; };
	struct Etr { using Data = DataA0; static constexpr Gpio::Signal Signal = Gpio::Signal::Etr; };
	struct In0 { using Data = DataA0; static constexpr Gpio::Signal Signal = Gpio::Signal::In0; };
	struct Wkup { using Data = DataA0; static constexpr Gpio::Signal Signal = Gpio::Signal::Wkup; };
};
template<Peripheral p> struct SignalConnection<DataA0::BitBang, p> {
	static_assert(p == Peripheral::BitBang, "GpioA0::BitBang only connects to software drivers!"); };
template<Peripheral p> struct SignalConnection<DataA0::Ch1, p> {
	static_assert((p == Peripheral::Tim2),"GpioA0::Ch1 only connects to Tim2!"); };
template<Peripheral p> struct SignalConnection<DataA0::Cts, p> {
	static_assert((p == Peripheral::Usart2),"GpioA0::Cts only connects to Usart2!"); };
template<Peripheral p> struct SignalConnection<DataA0::Etr, p> {
	static_assert((p == Peripheral::Tim2),"GpioA0::Etr only connects to Tim2!"); };
template<Peripheral p> struct SignalConnection<DataA0::In0, p> {
	static_assert((p == Peripheral::Adc1) or (p == Peripheral::Adc2),
		"GpioA0::In0 only connects to Adc1 or Adc2!"); };
template<Peripheral p> struct SignalConnection<DataA0::Wkup, p> {
	static_assert((p == Peripheral::Sys),"GpioA0::Wkup only connects to Sys!"); };
template<> struct SignalConnection<DataA0::BitBang, Peripheral::BitBang> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataA0::Ch1, Peripheral::Tim2> { 
	static constexpr uint32_t Groups = (1ul << 0) | (1ul << 2); static constexpr int8_t af = 0; };
template<> struct SignalConnection<DataA0::Cts, Peripheral::Usart2> { 
	static constexpr uint32_t Groups = (1ul << 0); static constexpr int8_t af = 0; };
template<> struct SignalConnection<DataA0::Etr, Peripheral::Tim2> { 
	static constexpr uint32_t Groups = (1ul << 0) | (1ul << 2); static constexpr int8_t af = 0; };
template<> struct SignalConnection<DataA0::In0, Peripheral::Adc1> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -2; };
template<> struct SignalConnection<DataA0::In0, Peripheral::Adc2> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -2; };
template<> struct SignalConnection<DataA0::Wkup, Peripheral::Sys> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> constexpr int8_t AdcChannel<DataA0, Peripheral::Adc1> = 0;
template<> constexpr int8_t AdcChannel<DataA0, Peripheral::Adc2> = 0;

struct DataA1 {
	static constexpr Gpio::Port port = Gpio::Port::A;
	static constexpr uint8_t pin = 1;
	struct BitBang { using Data = DataA1; static constexpr Gpio::Signal Signal = Gpio::Signal::BitBang; };
	struct Ch2 { using Data = DataA1; static constexpr Gpio::Signal Signal = Gpio::Signal::Ch2; };
	struct In1 { using Data = DataA1; static constexpr Gpio::Signal Signal = Gpio::Signal::In1; };
	struct Rts { using Data = DataA1; static constexpr Gpio::Signal Signal = Gpio::Signal::Rts; };
};
template<Peripheral p> struct SignalConnection<DataA1::BitBang, p> {
	static_assert(p == Peripheral::BitBang, "GpioA1::BitBang only connects to software drivers!"); };
template<Peripheral p> struct SignalConnection<DataA1::Ch2, p> {
	static_assert((p == Peripheral::Tim2),"GpioA1::Ch2 only connects to Tim2!"); };
template<Peripheral p> struct SignalConnection<DataA1::In1, p> {
	static_assert((p == Peripheral::Adc1) or (p == Peripheral::Adc2),
		"GpioA1::In1 only connects to Adc1 or Adc2!"); };
template<Peripheral p> struct SignalConnection<DataA1::Rts, p> {
	static_assert((p == Peripheral::Usart2),"GpioA1::Rts only connects to Usart2!"); };
template<> struct SignalConnection<DataA1::BitBang, Peripheral::BitBang> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataA1::Ch2, Peripheral::Tim2> { 
	static constexpr uint32_t Groups = (1ul << 0) | (1ul << 2); static constexpr int8_t af = 0; };
template<> struct SignalConnection<DataA1::In1, Peripheral::Adc1> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -2; };
template<> struct SignalConnection<DataA1::In1, Peripheral::Adc2> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -2; };
template<> struct SignalConnection<DataA1::Rts, Peripheral::Usart2> { 
	static constexpr uint32_t Groups = (1ul << 0); static constexpr int8_t af = 0; };
template<> constexpr int8_t AdcChannel<DataA1, Peripheral::Adc1> = 1;
template<> constexpr int8_t AdcChannel<DataA1, Peripheral::Adc2> = 1;

struct DataA2 {
	static constexpr Gpio::Port port = Gpio::Port::A;
	static constexpr uint8_t pin = 2;
	struct BitBang { using Data = DataA2; static constexpr Gpio::Signal Signal = Gpio::Signal::BitBang; };
	struct Ch3 { using Data = DataA2; static constexpr Gpio::Signal Signal = Gpio::Signal::Ch3; };
	struct In2 { using Data = DataA2; static constexpr Gpio::Signal Signal = Gpio::Signal::In2; };
	struct Tx { using Data = DataA2; static constexpr Gpio::Signal Signal = Gpio::Signal::Tx; };
};
template<Peripheral p> struct SignalConnection<DataA2::BitBang, p> {
	static_assert(p == Peripheral::BitBang, "GpioA2::BitBang only connects to software drivers!"); };
template<Peripheral p> struct SignalConnection<DataA2::Ch3, p> {
	static_assert((p == Peripheral::Tim2),"GpioA2::Ch3 only connects to Tim2!"); };
template<Peripheral p> struct SignalConnection<DataA2::In2, p> {
	static_assert((p == Peripheral::Adc1) or (p == Peripheral::Adc2),
		"GpioA2::In2 only connects to Adc1 or Adc2!"); };
template<Peripheral p> struct SignalConnection<DataA2::Tx, p> {
	static_assert((p == Peripheral::Usart2),"GpioA2::Tx only connects to Usart2!"); };
template<> struct SignalConnection<DataA2::BitBang, Peripheral::BitBang> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataA2::Ch3, Peripheral::Tim2> { 
	static constexpr uint32_t Groups = (1ul << 0) | (1ul << 1); static constexpr int8_t af = 0; };
template<> struct SignalConnection<DataA2::In2, Peripheral::Adc1> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -2; };
template<> struct SignalConnection<DataA2::In2, Peripheral::Adc2> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -2; };
template<> struct SignalConnection<DataA2::Tx, Peripheral::Usart2> { 
	static constexpr uint32_t Groups = (1ul << 0); static constexpr int8_t af = 0; };
template<> constexpr int8_t AdcChannel<DataA2, Peripheral::Adc1> = 2;
template<> constexpr int8_t AdcChannel<DataA2, Peripheral::Adc2> = 2;

struct DataA3 {
	static constexpr Gpio::Port port = Gpio::Port::A;
	static constexpr uint8_t pin = 3;
	struct BitBang { using Data = DataA3; static constexpr Gpio::Signal Signal = Gpio::Signal::BitBang; };
	struct Ch4 { using Data = DataA3; static constexpr Gpio::Signal Signal = Gpio::Signal::Ch4; };
	struct In3 { using Data = DataA3; static constexpr Gpio::Signal Signal = Gpio::Signal::In3; };
	struct Rx { using Data = DataA3; static constexpr Gpio::Signal Signal = Gpio::Signal::Rx; };
};
template<Peripheral p> struct SignalConnection<DataA3::BitBang, p> {
	static_assert(p == Peripheral::BitBang, "GpioA3::BitBang only connects to software drivers!"); };
template<Peripheral p> struct SignalConnection<DataA3::Ch4, p> {
	static_assert((p == Peripheral::Tim2),"GpioA3::Ch4 only connects to Tim2!"); };
template<Peripheral p> struct SignalConnection<DataA3::In3, p> {
	static_assert((p == Peripheral::Adc1) or (p == Peripheral::Adc2),
		"GpioA3::In3 only connects to Adc1 or Adc2!"); };
template<Peripheral p> struct SignalConnection<DataA3::Rx, p> {
	static_assert((p == Peripheral::Usart2),"GpioA3::Rx only connects to Usart2!"); };
template<> struct SignalConnection<DataA3::BitBang, Peripheral::BitBang> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataA3::Ch4, Peripheral::Tim2> { 
	static constexpr uint32_t Groups = (1ul << 0) | (1ul << 1); static constexpr int8_t af = 0; };
template<> struct SignalConnection<DataA3::In3, Peripheral::Adc1> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -2; };
template<> struct SignalConnection<DataA3::In3, Peripheral::Adc2> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -2; };
template<> struct SignalConnection<DataA3::Rx, Peripheral::Usart2> { 
	static constexpr uint32_t Groups = (1ul << 0); static constexpr int8_t af = 0; };
template<> constexpr int8_t AdcChannel<DataA3, Peripheral::Adc1> = 3;
template<> constexpr int8_t AdcChannel<DataA3, Peripheral::Adc2> = 3;

struct DataA4 {
	static constexpr Gpio::Port port = Gpio::Port::A;
	static constexpr uint8_t pin = 4;
	struct BitBang { using Data = DataA4; static constexpr Gpio::Signal Signal = Gpio::Signal::BitBang; };
	struct Ck { using Data = DataA4; static constexpr Gpio::Signal Signal = Gpio::Signal::Ck; };
	struct In4 { using Data = DataA4; static constexpr Gpio::Signal Signal = Gpio::Signal::In4; };
	struct Nss { using Data = DataA4; static constexpr Gpio::Signal Signal = Gpio::Signal::Nss; };
};
template<Peripheral p> struct SignalConnection<DataA4::BitBang, p> {
	static_assert(p == Peripheral::BitBang, "GpioA4::BitBang only connects to software drivers!"); };
template<Peripheral p> struct SignalConnection<DataA4::Ck, p> {
	static_assert((p == Peripheral::Usart2),"GpioA4::Ck only connects to Usart2!"); };
template<Peripheral p> struct SignalConnection<DataA4::In4, p> {
	static_assert((p == Peripheral::Adc1) or (p == Peripheral::Adc2),
		"GpioA4::In4 only connects to Adc1 or Adc2!"); };
template<Peripheral p> struct SignalConnection<DataA4::Nss, p> {
	static_assert((p == Peripheral::Spi1),"GpioA4::Nss only connects to Spi1!"); };
template<> struct SignalConnection<DataA4::BitBang, Peripheral::BitBang> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataA4::Ck, Peripheral::Usart2> { 
	static constexpr uint32_t Groups = (1ul << 0); static constexpr int8_t af = 0; };
template<> struct SignalConnection<DataA4::In4, Peripheral::Adc1> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -2; };
template<> struct SignalConnection<DataA4::In4, Peripheral::Adc2> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -2; };
template<> struct SignalConnection<DataA4::Nss, Peripheral::Spi1> { 
	static constexpr uint32_t Groups = (1ul << 0); static constexpr int8_t af = 0; };
template<> constexpr int8_t AdcChannel<DataA4, Peripheral::Adc1> = 4;
template<> constexpr int8_t AdcChannel<DataA4, Peripheral::Adc2> = 4;

struct DataA5 {
	static constexpr Gpio::Port port = Gpio::Port::A;
	static constexpr uint8_t pin = 5;
	struct BitBang { using Data = DataA5; static constexpr Gpio::Signal Signal = Gpio::Signal::BitBang; };
	struct In5 { using Data = DataA5; static constexpr Gpio::Signal Signal = Gpio::Signal::In5; };
	struct Sck { using Data = DataA5; static constexpr Gpio::Signal Signal = Gpio::Signal::Sck; };
};
template<Peripheral p> struct SignalConnection<DataA5::BitBang, p> {
	static_assert(p == Peripheral::BitBang, "GpioA5::BitBang only connects to software drivers!"); };
template<Peripheral p> struct SignalConnection<DataA5::In5, p> {
	static_assert((p == Peripheral::Adc1) or (p == Peripheral::Adc2),
		"GpioA5::In5 only connects to Adc1 or Adc2!"); };
template<Peripheral p> struct SignalConnection<DataA5::Sck, p> {
	static_assert((p == Peripheral::Spi1),"GpioA5::Sck only connects to Spi1!"); };
template<> struct SignalConnection<DataA5::BitBang, Peripheral::BitBang> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataA5::In5, Peripheral::Adc1> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -2; };
template<> struct SignalConnection<DataA5::In5, Peripheral::Adc2> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -2; };
template<> struct SignalConnection<DataA5::Sck, Peripheral::Spi1> { 
	static constexpr uint32_t Groups = (1ul << 0); static constexpr int8_t af = 0; };
template<> constexpr int8_t AdcChannel<DataA5, Peripheral::Adc1> = 5;
template<> constexpr int8_t AdcChannel<DataA5, Peripheral::Adc2> = 5;

struct DataA6 {
	static constexpr Gpio::Port port = Gpio::Port::A;
	static constexpr uint8_t pin = 6;
	struct BitBang { using Data = DataA6; static constexpr Gpio::Signal Signal = Gpio::Signal::BitBang; };
	struct Bkin { using Data = DataA6; static constexpr Gpio::Signal Signal = Gpio::Signal::Bkin; };
	struct Ch1 { using Data = DataA6; static constexpr Gpio::Signal Signal = Gpio::Signal::Ch1; };
	struct In6 { using Data = DataA6; static constexpr Gpio::Signal Signal = Gpio::Signal::In6; };
	struct Miso { using Data = DataA6; static constexpr Gpio::Signal Signal = Gpio::Signal::Miso; };
};
template<Peripheral p> struct SignalConnection<DataA6::BitBang, p> {
	static_assert(p == Peripheral::BitBang, "GpioA6::BitBang only connects to software drivers!"); };
template<Peripheral p> struct SignalConnection<DataA6::Bkin, p> {
	static_assert((p == Peripheral::Tim1),"GpioA6::Bkin only connects to Tim1!"); };
template<Peripheral p> struct SignalConnection<DataA6::Ch1, p> {
	static_assert((p == Peripheral::Tim3),"GpioA6::Ch1 only connects to Tim3!"); };
template<Peripheral p> struct SignalConnection<DataA6::In6, p> {
	static_assert((p == Peripheral::Adc1) or (p == Peripheral::Adc2),
		"GpioA6::In6 only connects to Adc1 or Adc2!"); };
template<Peripheral p> struct SignalConnection<DataA6::Miso, p> {
	static_assert((p == Peripheral::Spi1),"GpioA6::Miso only connects to Spi1!"); };
template<> struct SignalConnection<DataA6::BitBang, Peripheral::BitBang> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataA6::Bkin, Peripheral::Tim1> { 
	static constexpr uint32_t Groups = (1ul << 1); static constexpr int8_t af = 1; };
template<> struct SignalConnection<DataA6::Ch1, Peripheral::Tim3> { 
	static constexpr uint32_t Groups = (1ul << 0); static constexpr int8_t af = 0; };
template<> struct SignalConnection<DataA6::In6, Peripheral::Adc1> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -2; };
template<> struct SignalConnection<DataA6::In6, Peripheral::Adc2> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -2; };
template<> struct SignalConnection<DataA6::Miso, Peripheral::Spi1> { 
	static constexpr uint32_t Groups = (1ul << 0); static constexpr int8_t af = 0; };
template<> constexpr int8_t AdcChannel<DataA6, Peripheral::Adc1> = 6;
template<> constexpr int8_t AdcChannel<DataA6, Peripheral::Adc2> = 6;

struct DataA7 {
	static constexpr Gpio::Port port = Gpio::Port::A;
	static constexpr uint8_t pin = 7;
	struct BitBang { using Data = DataA7; static constexpr Gpio::Signal Signal = Gpio::Signal::BitBang; };
	struct Ch1n { using Data = DataA7; static constexpr Gpio::Signal Signal = Gpio::Signal::Ch1n; };
	struct Ch2 { using Data = DataA7; static constexpr Gpio::Signal Signal = Gpio::Signal::Ch2; };
	struct In7 { using Data = DataA7; static constexpr Gpio::Signal Signal = Gpio::Signal::In7; };
	struct Mosi { using Data = DataA7; static constexpr Gpio::Signal Signal = Gpio::Signal::Mosi; };
};
template<Peripheral p> struct SignalConnection<DataA7::BitBang, p> {
	static_assert(p == Peripheral::BitBang, "GpioA7::BitBang only connects to software drivers!"); };
template<Peripheral p> struct SignalConnection<DataA7::Ch1n, p> {
	static_assert((p == Peripheral::Tim1),"GpioA7::Ch1n only connects to Tim1!"); };
template<Peripheral p> struct SignalConnection<DataA7::Ch2, p> {
	static_assert((p == Peripheral::Tim3),"GpioA7::Ch2 only connects to Tim3!"); };
template<Peripheral p> struct SignalConnection<DataA7::In7, p> {
	static_assert((p == Peripheral::Adc1) or (p == Peripheral::Adc2),
		"GpioA7::In7 only connects to Adc1 or Adc2!"); };
template<Peripheral p> struct SignalConnection<DataA7::Mosi, p> {
	static_assert((p == Peripheral::Spi1),"GpioA7::Mosi only connects to Spi1!"); };
template<> struct SignalConnection<DataA7::BitBang, Peripheral::BitBang> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataA7::Ch1n, Peripheral::Tim1> { 
	static constexpr uint32_t Groups = (1ul << 1); static constexpr int8_t af = 1; };
template<> struct SignalConnection<DataA7::Ch2, Peripheral::Tim3> { 
	static constexpr uint32_t Groups = (1ul << 0); static constexpr int8_t af = 0; };
template<> struct SignalConnection<DataA7::In7, Peripheral::Adc1> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -2; };
template<> struct SignalConnection<DataA7::In7, Peripheral::Adc2> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -2; };
template<> struct SignalConnection<DataA7::Mosi, Peripheral::Spi1> { 
	static constexpr uint32_t Groups = (1ul << 0); static constexpr int8_t af = 0; };
template<> constexpr int8_t AdcChannel<DataA7, Peripheral::Adc1> = 7;
template<> constexpr int8_t AdcChannel<DataA7, Peripheral::Adc2> = 7;

struct DataA8 {
	static constexpr Gpio::Port port = Gpio::Port::A;
	static constexpr uint8_t pin = 8;
	struct BitBang { using Data = DataA8; static constexpr Gpio::Signal Signal = Gpio::Signal::BitBang; };
	struct Ch1 { using Data = DataA8; static constexpr Gpio::Signal Signal = Gpio::Signal::Ch1; };
	struct Ck { using Data = DataA8; static constexpr Gpio::Signal Signal = Gpio::Signal::Ck; };
	struct Mco { using Data = DataA8; static constexpr Gpio::Signal Signal = Gpio::Signal::Mco; };
};
template<Peripheral p> struct SignalConnection<DataA8::BitBang, p> {
	static_assert(p == Peripheral::BitBang, "GpioA8::BitBang only connects to software drivers!"); };
template<Peripheral p> struct SignalConnection<DataA8::Ch1, p> {
	static_assert((p == Peripheral::Tim1),"GpioA8::Ch1 only connects to Tim1!"); };
template<Peripheral p> struct SignalConnection<DataA8::Ck, p> {
	static_assert((p == Peripheral::Usart1),"GpioA8::Ck only connects to Usart1!"); };
template<Peripheral p> struct SignalConnection<DataA8::Mco, p> {
	static_assert((p == Peripheral::Rcc),"GpioA8::Mco only connects to Rcc!"); };
template<> struct SignalConnection<DataA8::BitBang, Peripheral::BitBang> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataA8::Ch1, Peripheral::Tim1> { 
	static constexpr uint32_t Groups = (1ul << 0) | (1ul << 1); static constexpr int8_t af = 0; };
template<> struct SignalConnection<DataA8::Ck, Peripheral::Usart1> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataA8::Mco, Peripheral::Rcc> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };

struct DataA9 {
	static constexpr Gpio::Port port = Gpio::Port::A;
	static constexpr uint8_t pin = 9;
	struct BitBang { using Data = DataA9; static constexpr Gpio::Signal Signal = Gpio::Signal::BitBang; };
	struct Ch2 { using Data = DataA9; static constexpr Gpio::Signal Signal = Gpio::Signal::Ch2; };
	struct Tx { using Data = DataA9; static constexpr Gpio::Signal Signal = Gpio::Signal::Tx; };
};
template<Peripheral p> struct SignalConnection<DataA9::BitBang, p> {
	static_assert(p == Peripheral::BitBang, "GpioA9::BitBang only connects to software drivers!"); };
template<Peripheral p> struct SignalConnection<DataA9::Ch2, p> {
	static_assert((p == Peripheral::Tim1),"GpioA9::Ch2 only connects to Tim1!"); };
template<Peripheral p> struct SignalConnection<DataA9::Tx, p> {
	static_assert((p == Peripheral::Usart1),"GpioA9::Tx only connects to Usart1!"); };
template<> struct SignalConnection<DataA9::BitBang, Peripheral::BitBang> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataA9::Ch2, Peripheral::Tim1> { 
	static constexpr uint32_t Groups = (1ul << 0) | (1ul << 1); static constexpr int8_t af = 0; };
template<> struct SignalConnection<DataA9::Tx, Peripheral::Usart1> { 
	static constexpr uint32_t Groups = (1ul << 0); static constexpr int8_t af = 0; };

struct DataA10 {
	static constexpr Gpio::Port port = Gpio::Port::A;
	static constexpr uint8_t pin = 10;
	struct BitBang { using Data = DataA10; static constexpr Gpio::Signal Signal = Gpio::Signal::BitBang; };
	struct Ch3 { using Data = DataA10; static constexpr Gpio::Signal Signal = Gpio::Signal::Ch3; };
	struct Rx { using Data = DataA10; static constexpr Gpio::Signal Signal = Gpio::Signal::Rx; };
};
template<Peripheral p> struct SignalConnection<DataA10::BitBang, p> {
	static_assert(p == Peripheral::BitBang, "GpioA10::BitBang only connects to software drivers!"); };
template<Peripheral p> struct SignalConnection<DataA10::Ch3, p> {
	static_assert((p == Peripheral::Tim1),"GpioA10::Ch3 only connects to Tim1!"); };
template<Peripheral p> struct SignalConnection<DataA10::Rx, p> {
	static_assert((p == Peripheral::Usart1),"GpioA10::Rx only connects to Usart1!"); };
template<> struct SignalConnection<DataA10::BitBang, Peripheral::BitBang> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataA10::Ch3, Peripheral::Tim1> { 
	static constexpr uint32_t Groups = (1ul << 0) | (1ul << 1); static constexpr int8_t af = 0; };
template<> struct SignalConnection<DataA10::Rx, Peripheral::Usart1> { 
	static constexpr uint32_t Groups = (1ul << 0); static constexpr int8_t af = 0; };

struct DataA11 {
	static constexpr Gpio::Port port = Gpio::Port::A;
	static constexpr uint8_t pin = 11;
	struct BitBang { using Data = DataA11; static constexpr Gpio::Signal Signal = Gpio::Signal::BitBang; };
	struct Ch4 { using Data = DataA11; static constexpr Gpio::Signal Signal = Gpio::Signal::Ch4; };
	struct Cts { using Data = DataA11; static constexpr Gpio::Signal Signal = Gpio::Signal::Cts; };
	struct Dm { using Data = DataA11; static constexpr Gpio::Signal Signal = Gpio::Signal::Dm; };
	struct Rx { using Data = DataA11; static constexpr Gpio::Signal Signal = Gpio::Signal::Rx; };
};
template<Peripheral p> struct SignalConnection<DataA11::BitBang, p> {
	static_assert(p == Peripheral::BitBang, "GpioA11::BitBang only connects to software drivers!"); };
template<Peripheral p> struct SignalConnection<DataA11::Ch4, p> {
	static_assert((p == Peripheral::Tim1),"GpioA11::Ch4 only connects to Tim1!"); };
template<Peripheral p> struct SignalConnection<DataA11::Cts, p> {
	static_assert((p == Peripheral::Usart1),"GpioA11::Cts only connects to Usart1!"); };
template<Peripheral p> struct SignalConnection<DataA11::Dm, p> {
	static_assert((p == Peripheral::Usb),"GpioA11::Dm only connects to Usb!"); };
template<Peripheral p> struct SignalConnection<DataA11::Rx, p> {
	static_assert((p == Peripheral::Can),"GpioA11::Rx only connects to Can!"); };
template<> struct SignalConnection<DataA11::BitBang, Peripheral::BitBang> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataA11::Ch4, Peripheral::Tim1> { 
	static constexpr uint32_t Groups = (1ul << 0) | (1ul << 1); static constexpr int8_t af = 0; };
template<> struct SignalConnection<DataA11::Cts, Peripheral::Usart1> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataA11::Dm, Peripheral::Usb> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataA11::Rx, Peripheral::Can> { 
	static constexpr uint32_t Groups = (1ul << 0); static constexpr int8_t af = 0; };

struct DataA12 {
	static constexpr Gpio::Port port = Gpio::Port::A;
	static constexpr uint8_t pin = 12;
	struct BitBang { using Data = DataA12; static constexpr Gpio::Signal Signal = Gpio::Signal::BitBang; };
	struct Dp { using Data = DataA12; static constexpr Gpio::Signal Signal = Gpio::Signal::Dp; };
	struct Etr { using Data = DataA12; static constexpr Gpio::Signal Signal = Gpio::Signal::Etr; };
	struct Rts { using Data = DataA12; static constexpr Gpio::Signal Signal = Gpio::Signal::Rts; };
	struct Tx { using Data = DataA12; static constexpr Gpio::Signal Signal = Gpio::Signal::Tx; };
};
template<Peripheral p> struct SignalConnection<DataA12::BitBang, p> {
	static_assert(p == Peripheral::BitBang, "GpioA12::BitBang only connects to software drivers!"); };
template<Peripheral p> struct SignalConnection<DataA12::Dp, p> {
	static_assert((p == Peripheral::Usb),"GpioA12::Dp only connects to Usb!"); };
template<Peripheral p> struct SignalConnection<DataA12::Etr, p> {
	static_assert((p == Peripheral::Tim1),"GpioA12::Etr only connects to Tim1!"); };
template<Peripheral p> struct SignalConnection<DataA12::Rts, p> {
	static_assert((p == Peripheral::Usart1),"GpioA12::Rts only connects to Usart1!"); };
template<Peripheral p> struct SignalConnection<DataA12::Tx, p> {
	static_assert((p == Peripheral::Can),"GpioA12::Tx only connects to Can!"); };
template<> struct SignalConnection<DataA12::BitBang, Peripheral::BitBang> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataA12::Dp, Peripheral::Usb> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataA12::Etr, Peripheral::Tim1> { 
	static constexpr uint32_t Groups = (1ul << 0) | (1ul << 1); static constexpr int8_t af = 0; };
template<> struct SignalConnection<DataA12::Rts, Peripheral::Usart1> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataA12::Tx, Peripheral::Can> { 
	static constexpr uint32_t Groups = (1ul << 0); static constexpr int8_t af = 0; };

struct DataA13 {
	static constexpr Gpio::Port port = Gpio::Port::A;
	static constexpr uint8_t pin = 13;
	struct BitBang { using Data = DataA13; static constexpr Gpio::Signal Signal = Gpio::Signal::BitBang; };
	struct Jtms { using Data = DataA13; static constexpr Gpio::Signal Signal = Gpio::Signal::Jtms; };
	struct Swdio { using Data = DataA13; static constexpr Gpio::Signal Signal = Gpio::Signal::Swdio; };
};
template<Peripheral p> struct SignalConnection<DataA13::BitBang, p> {
	static_assert(p == Peripheral::BitBang, "GpioA13::BitBang only connects to software drivers!"); };
template<Peripheral p> struct SignalConnection<DataA13::Jtms, p> {
	static_assert((p == Peripheral::Sys),"GpioA13::Jtms only connects to Sys!"); };
template<Peripheral p> struct SignalConnection<DataA13::Swdio, p> {
	static_assert((p == Peripheral::Sys),"GpioA13::Swdio only connects to Sys!"); };
template<> struct SignalConnection<DataA13::BitBang, Peripheral::BitBang> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataA13::Jtms, Peripheral::Sys> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataA13::Swdio, Peripheral::Sys> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };

struct DataA14 {
	static constexpr Gpio::Port port = Gpio::Port::A;
	static constexpr uint8_t pin = 14;
	struct BitBang { using Data = DataA14; static constexpr Gpio::Signal Signal = Gpio::Signal::BitBang; };
	struct Jtck { using Data = DataA14; static constexpr Gpio::Signal Signal = Gpio::Signal::Jtck; };
	struct Swclk { using Data = DataA14; static constexpr Gpio::Signal Signal = Gpio::Signal::Swclk; };
};
template<Peripheral p> struct SignalConnection<DataA14::BitBang, p> {
	static_assert(p == Peripheral::BitBang, "GpioA14::BitBang only connects to software drivers!"); };
template<Peripheral p> struct SignalConnection<DataA14::Jtck, p> {
	static_assert((p == Peripheral::Sys),"GpioA14::Jtck only connects to Sys!"); };
template<Peripheral p> struct SignalConnection<DataA14::Swclk, p> {
	static_assert((p == Peripheral::Sys),"GpioA14::Swclk only connects to Sys!"); };
template<> struct SignalConnection<DataA14::BitBang, Peripheral::BitBang> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataA14::Jtck, Peripheral::Sys> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataA14::Swclk, Peripheral::Sys> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };

struct DataA15 {
	static constexpr Gpio::Port port = Gpio::Port::A;
	static constexpr uint8_t pin = 15;
	struct BitBang { using Data = DataA15; static constexpr Gpio::Signal Signal = Gpio::Signal::BitBang; };
	struct Ch1 { using Data = DataA15; static constexpr Gpio::Signal Signal = Gpio::Signal::Ch1; };
	struct Etr { using Data = DataA15; static constexpr Gpio::Signal Signal = Gpio::Signal::Etr; };
	struct Jtdi { using Data = DataA15; static constexpr Gpio::Signal Signal = Gpio::Signal::Jtdi; };
	struct Nss { using Data = DataA15; static constexpr Gpio::Signal Signal = Gpio::Signal::Nss; };
};
template<Peripheral p> struct SignalConnection<DataA15::BitBang, p> {
	static_assert(p == Peripheral::BitBang, "GpioA15::BitBang only connects to software drivers!"); };
template<Peripheral p> struct SignalConnection<DataA15::Ch1, p> {
	static_assert((p == Peripheral::Tim2),"GpioA15::Ch1 only connects to Tim2!"); };
template<Peripheral p> struct SignalConnection<DataA15::Etr, p> {
	static_assert((p == Peripheral::Tim2),"GpioA15::Etr only connects to Tim2!"); };
template<Peripheral p> struct SignalConnection<DataA15::Jtdi, p> {
	static_assert((p == Peripheral::Sys),"GpioA15::Jtdi only connects to Sys!"); };
template<Peripheral p> struct SignalConnection<DataA15::Nss, p> {
	static_assert((p == Peripheral::Spi1),"GpioA15::Nss only connects to Spi1!"); };
template<> struct SignalConnection<DataA15::BitBang, Peripheral::BitBang> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataA15::Ch1, Peripheral::Tim2> { 
	static constexpr uint32_t Groups = (1ul << 1) | (1ul << 3); static constexpr int8_t af = 1; };
template<> struct SignalConnection<DataA15::Etr, Peripheral::Tim2> { 
	static constexpr uint32_t Groups = (1ul << 1) | (1ul << 3); static constexpr int8_t af = 1; };
template<> struct SignalConnection<DataA15::Jtdi, Peripheral::Sys> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataA15::Nss, Peripheral::Spi1> { 
	static constexpr uint32_t Groups = (1ul << 1); static constexpr int8_t af = 1; };

struct DataB0 {
	static constexpr Gpio::Port port = Gpio::Port::B;
	static constexpr uint8_t pin = 0;
	struct BitBang { using Data = DataB0; static constexpr Gpio::Signal Signal = Gpio::Signal::BitBang; };
	struct Ch2n { using Data = DataB0; static constexpr Gpio::Signal Signal = Gpio::Signal::Ch2n; };
	struct Ch3 { using Data = DataB0; static constexpr Gpio::Signal Signal = Gpio::Signal::Ch3; };
	struct In8 { using Data = DataB0; static constexpr Gpio::Signal Signal = Gpio::Signal::In8; };
};
template<Peripheral p> struct SignalConnection<DataB0::BitBang, p> {
	static_assert(p == Peripheral::BitBang, "GpioB0::BitBang only connects to software drivers!"); };
template<Peripheral p> struct SignalConnection<DataB0::Ch2n, p> {
	static_assert((p == Peripheral::Tim1),"GpioB0::Ch2n only connects to Tim1!"); };
template<Peripheral p> struct SignalConnection<DataB0::Ch3, p> {
	static_assert((p == Peripheral::Tim3),"GpioB0::Ch3 only connects to Tim3!"); };
template<Peripheral p> struct SignalConnection<DataB0::In8, p> {
	static_assert((p == Peripheral::Adc1) or (p == Peripheral::Adc2),
		"GpioB0::In8 only connects to Adc1 or Adc2!"); };
template<> struct SignalConnection<DataB0::BitBang, Peripheral::BitBang> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataB0::Ch2n, Peripheral::Tim1> { 
	static constexpr uint32_t Groups = (1ul << 1); static constexpr int8_t af = 1; };
template<> struct SignalConnection<DataB0::Ch3, Peripheral::Tim3> { 
	static constexpr uint32_t Groups = (1ul << 0) | (1ul << 2); static constexpr int8_t af = 0; };
template<> struct SignalConnection<DataB0::In8, Peripheral::Adc1> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -2; };
template<> struct SignalConnection<DataB0::In8, Peripheral::Adc2> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -2; };
template<> constexpr int8_t AdcChannel<DataB0, Peripheral::Adc1> = 8;
template<> constexpr int8_t AdcChannel<DataB0, Peripheral::Adc2> = 8;

struct DataB1 {
	static constexpr Gpio::Port port = Gpio::Port::B;
	static constexpr uint8_t pin = 1;
	struct BitBang { using Data = DataB1; static constexpr Gpio::Signal Signal = Gpio::Signal::BitBang; };
	struct Ch3n { using Data = DataB1; static constexpr Gpio::Signal Signal = Gpio::Signal::Ch3n; };
	struct Ch4 { using Data = DataB1; static constexpr Gpio::Signal Signal = Gpio::Signal::Ch4; };
	struct In9 { using Data = DataB1; static constexpr Gpio::Signal Signal = Gpio::Signal::In9; };
};
template<Peripheral p> struct SignalConnection<DataB1::BitBang, p> {
	static_assert(p == Peripheral::BitBang, "GpioB1::BitBang only connects to software drivers!"); };
template<Peripheral p> struct SignalConnection<DataB1::Ch3n, p> {
	static_assert((p == Peripheral::Tim1),"GpioB1::Ch3n only connects to Tim1!"); };
template<Peripheral p> struct SignalConnection<DataB1::Ch4, p> {
	static_assert((p == Peripheral::Tim3),"GpioB1::Ch4 only connects to Tim3!"); };
template<Peripheral p> struct SignalConnection<DataB1::In9, p> {
	static_assert((p == Peripheral::Adc1) or (p == Peripheral::Adc2),
		"GpioB1::In9 only connects to Adc1 or Adc2!"); };
template<> struct SignalConnection<DataB1::BitBang, Peripheral::BitBang> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataB1::Ch3n, Peripheral::Tim1> { 
	static constexpr uint32_t Groups = (1ul << 1); static constexpr int8_t af = 1; };
template<> struct SignalConnection<DataB1::Ch4, Peripheral::Tim3> { 
	static constexpr uint32_t Groups = (1ul << 0) | (1ul << 2); static constexpr int8_t af = 0; };
template<> struct SignalConnection<DataB1::In9, Peripheral::Adc1> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -2; };
template<> struct SignalConnection<DataB1::In9, Peripheral::Adc2> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -2; };
template<> constexpr int8_t AdcChannel<DataB1, Peripheral::Adc1> = 9;
template<> constexpr int8_t AdcChannel<DataB1, Peripheral::Adc2> = 9;

struct DataB2 {
	static constexpr Gpio::Port port = Gpio::Port::B;
	static constexpr uint8_t pin = 2;
	struct BitBang { using Data = DataB2; static constexpr Gpio::Signal Signal = Gpio::Signal::BitBang; };
};
template<Peripheral p> struct SignalConnection<DataB2::BitBang, p> {
	static_assert(p == Peripheral::BitBang, "GpioB2::BitBang only connects to software drivers!"); };
template<> struct SignalConnection<DataB2::BitBang, Peripheral::BitBang> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };

struct DataB3 {
	static constexpr Gpio::Port port = Gpio::Port::B;
	static constexpr uint8_t pin = 3;
	struct BitBang { using Data = DataB3; static constexpr Gpio::Signal Signal = Gpio::Signal::BitBang; };
	struct Ch2 { using Data = DataB3; static constexpr Gpio::Signal Signal = Gpio::Signal::Ch2; };
	struct Jtdo { using Data = DataB3; static constexpr Gpio::Signal Signal = Gpio::Signal::Jtdo; };
	struct Sck { using Data = DataB3; static constexpr Gpio::Signal Signal = Gpio::Signal::Sck; };
	struct Traceswo { using Data = DataB3; static constexpr Gpio::Signal Signal = Gpio::Signal::Traceswo; };
};
template<Peripheral p> struct SignalConnection<DataB3::BitBang, p> {
	static_assert(p == Peripheral::BitBang, "GpioB3::BitBang only connects to software drivers!"); };
template<Peripheral p> struct SignalConnection<DataB3::Ch2, p> {
	static_assert((p == Peripheral::Tim2),"GpioB3::Ch2 only connects to Tim2!"); };
template<Peripheral p> struct SignalConnection<DataB3::Jtdo, p> {
	static_assert((p == Peripheral::Sys),"GpioB3::Jtdo only connects to Sys!"); };
template<Peripheral p> struct SignalConnection<DataB3::Sck, p> {
	static_assert((p == Peripheral::Spi1),"GpioB3::Sck only connects to Spi1!"); };
template<Peripheral p> struct SignalConnection<DataB3::Traceswo, p> {
	static_assert((p == Peripheral::Sys),"GpioB3::Traceswo only connects to Sys!"); };
template<> struct SignalConnection<DataB3::BitBang, Peripheral::BitBang> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataB3::Ch2, Peripheral::Tim2> { 
	static constexpr uint32_t Groups = (1ul << 1) | (1ul << 3); static constexpr int8_t af = 1; };
template<> struct SignalConnection<DataB3::Jtdo, Peripheral::Sys> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataB3::Sck, Peripheral::Spi1> { 
	static constexpr uint32_t Groups = (1ul << 1); static constexpr int8_t af = 1; };
template<> struct SignalConnection<DataB3::Traceswo, Peripheral::Sys> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };

struct DataB4 {
	static constexpr Gpio::Port port = Gpio::Port::B;
	static constexpr uint8_t pin = 4;
	struct BitBang { using Data = DataB4; static constexpr Gpio::Signal Signal = Gpio::Signal::BitBang; };
	struct Ch1 { using Data = DataB4; static constexpr Gpio::Signal Signal = Gpio::Signal::Ch1; };
	struct Miso { using Data = DataB4; static constexpr Gpio::Signal Signal = Gpio::Signal::Miso; };
	struct Njtrst { using Data = DataB4; static constexpr Gpio::Signal Signal = Gpio::Signal::Njtrst; };
};
template<Peripheral p> struct SignalConnection<DataB4::BitBang, p> {
	static_assert(p == Peripheral::BitBang, "GpioB4::BitBang only connects to software drivers!"); };
template<Peripheral p> struct SignalConnection<DataB4::Ch1, p> {
	static_assert((p == Peripheral::Tim3),"GpioB4::Ch1 only connects to Tim3!"); };
template<Peripheral p> struct SignalConnection<DataB4::Miso, p> {
	static_assert((p == Peripheral::Spi1),"GpioB4::Miso only connects to Spi1!"); };
template<Peripheral p> struct SignalConnection<DataB4::Njtrst, p> {
	static_assert((p == Peripheral::Sys),"GpioB4::Njtrst only connects to Sys!"); };
template<> struct SignalConnection<DataB4::BitBang, Peripheral::BitBang> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataB4::Ch1, Peripheral::Tim3> { 
	static constexpr uint32_t Groups = (1ul << 2); static constexpr int8_t af = 2; };
template<> struct SignalConnection<DataB4::Miso, Peripheral::Spi1> { 
	static constexpr uint32_t Groups = (1ul << 1); static constexpr int8_t af = 1; };
template<> struct SignalConnection<DataB4::Njtrst, Peripheral::Sys> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };

struct DataB5 {
	static constexpr Gpio::Port port = Gpio::Port::B;
	static constexpr uint8_t pin = 5;
	struct BitBang { using Data = DataB5; static constexpr Gpio::Signal Signal = Gpio::Signal::BitBang; };
	struct Ch2 { using Data = DataB5; static constexpr Gpio::Signal Signal = Gpio::Signal::Ch2; };
	struct Mosi { using Data = DataB5; static constexpr Gpio::Signal Signal = Gpio::Signal::Mosi; };
	struct Smba { using Data = DataB5; static constexpr Gpio::Signal Signal = Gpio::Signal::Smba; };
};
template<Peripheral p> struct SignalConnection<DataB5::BitBang, p> {
	static_assert(p == Peripheral::BitBang, "GpioB5::BitBang only connects to software drivers!"); };
template<Peripheral p> struct SignalConnection<DataB5::Ch2, p> {
	static_assert((p == Peripheral::Tim3),"GpioB5::Ch2 only connects to Tim3!"); };
template<Peripheral p> struct SignalConnection<DataB5::Mosi, p> {
	static_assert((p == Peripheral::Spi1),"GpioB5::Mosi only connects to Spi1!"); };
template<Peripheral p> struct SignalConnection<DataB5::Smba, p> {
	static_assert((p == Peripheral::I2c1),"GpioB5::Smba only connects to I2c1!"); };
template<> struct SignalConnection<DataB5::BitBang, Peripheral::BitBang> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataB5::Ch2, Peripheral::Tim3> { 
	static constexpr uint32_t Groups = (1ul << 2); static constexpr int8_t af = 2; };
template<> struct SignalConnection<DataB5::Mosi, Peripheral::Spi1> { 
	static constexpr uint32_t Groups = (1ul << 1); static constexpr int8_t af = 1; };
template<> struct SignalConnection<DataB5::Smba, Peripheral::I2c1> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };

struct DataB6 {
	static constexpr Gpio::Port port = Gpio::Port::B;
	static constexpr uint8_t pin = 6;
	struct BitBang { using Data = DataB6; static constexpr Gpio::Signal Signal = Gpio::Signal::BitBang; };
	struct Ch1 { using Data = DataB6; static constexpr Gpio::Signal Signal = Gpio::Signal::Ch1; };
	struct Scl { using Data = DataB6; static constexpr Gpio::Signal Signal = Gpio::Signal::Scl; };
	struct Tx { using Data = DataB6; static constexpr Gpio::Signal Signal = Gpio::Signal::Tx; };
};
template<Peripheral p> struct SignalConnection<DataB6::BitBang, p> {
	static_assert(p == Peripheral::BitBang, "GpioB6::BitBang only connects to software drivers!"); };
template<Peripheral p> struct SignalConnection<DataB6::Ch1, p> {
	static_assert((p == Peripheral::Tim4),"GpioB6::Ch1 only connects to Tim4!"); };
template<Peripheral p> struct SignalConnection<DataB6::Scl, p> {
	static_assert((p == Peripheral::I2c1),"GpioB6::Scl only connects to I2c1!"); };
template<Peripheral p> struct SignalConnection<DataB6::Tx, p> {
	static_assert((p == Peripheral::Usart1),"GpioB6::Tx only connects to Usart1!"); };
template<> struct SignalConnection<DataB6::BitBang, Peripheral::BitBang> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataB6::Ch1, Peripheral::Tim4> { 
	static constexpr uint32_t Groups = (1ul << 0); static constexpr int8_t af = 0; };
template<> struct SignalConnection<DataB6::Scl, Peripheral::I2c1> { 
	static constexpr uint32_t Groups = (1ul << 0); static constexpr int8_t af = 0; };
template<> struct SignalConnection<DataB6::Tx, Peripheral::Usart1> { 
	static constexpr uint32_t Groups = (1ul << 1); static constexpr int8_t af = 1; };

struct DataB7 {
	static constexpr Gpio::Port port = Gpio::Port::B;
	static constexpr uint8_t pin = 7;
	struct BitBang { using Data = DataB7; static constexpr Gpio::Signal Signal = Gpio::Signal::BitBang; };
	struct Ch2 { using Data = DataB7; static constexpr Gpio::Signal Signal = Gpio::Signal::Ch2; };
	struct Rx { using Data = DataB7; static constexpr Gpio::Signal Signal = Gpio::Signal::Rx; };
	struct Sda { using Data = DataB7; static constexpr Gpio::Signal Signal = Gpio::Signal::Sda; };
};
template<Peripheral p> struct SignalConnection<DataB7::BitBang, p> {
	static_assert(p == Peripheral::BitBang, "GpioB7::BitBang only connects to software drivers!"); };
template<Peripheral p> struct SignalConnection<DataB7::Ch2, p> {
	static_assert((p == Peripheral::Tim4),"GpioB7::Ch2 only connects to Tim4!"); };
template<Peripheral p> struct SignalConnection<DataB7::Rx, p> {
	static_assert((p == Peripheral::Usart1),"GpioB7::Rx only connects to Usart1!"); };
template<Peripheral p> struct SignalConnection<DataB7::Sda, p> {
	static_assert((p == Peripheral::I2c1),"GpioB7::Sda only connects to I2c1!"); };
template<> struct SignalConnection<DataB7::BitBang, Peripheral::BitBang> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataB7::Ch2, Peripheral::Tim4> { 
	static constexpr uint32_t Groups = (1ul << 0); static constexpr int8_t af = 0; };
template<> struct SignalConnection<DataB7::Rx, Peripheral::Usart1> { 
	static constexpr uint32_t Groups = (1ul << 1); static constexpr int8_t af = 1; };
template<> struct SignalConnection<DataB7::Sda, Peripheral::I2c1> { 
	static constexpr uint32_t Groups = (1ul << 0); static constexpr int8_t af = 0; };

struct DataB8 {
	static constexpr Gpio::Port port = Gpio::Port::B;
	static constexpr uint8_t pin = 8;
	struct BitBang { using Data = DataB8; static constexpr Gpio::Signal Signal = Gpio::Signal::BitBang; };
	struct Ch3 { using Data = DataB8; static constexpr Gpio::Signal Signal = Gpio::Signal::Ch3; };
	struct Rx { using Data = DataB8; static constexpr Gpio::Signal Signal = Gpio::Signal::Rx; };
	struct Scl { using Data = DataB8; static constexpr Gpio::Signal Signal = Gpio::Signal::Scl; };
};
template<Peripheral p> struct SignalConnection<DataB8::BitBang, p> {
	static_assert(p == Peripheral::BitBang, "GpioB8::BitBang only connects to software drivers!"); };
template<Peripheral p> struct SignalConnection<DataB8::Ch3, p> {
	static_assert((p == Peripheral::Tim4),"GpioB8::Ch3 only connects to Tim4!"); };
template<Peripheral p> struct SignalConnection<DataB8::Rx, p> {
	static_assert((p == Peripheral::Can),"GpioB8::Rx only connects to Can!"); };
template<Peripheral p> struct SignalConnection<DataB8::Scl, p> {
	static_assert((p == Peripheral::I2c1),"GpioB8::Scl only connects to I2c1!"); };
template<> struct SignalConnection<DataB8::BitBang, Peripheral::BitBang> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataB8::Ch3, Peripheral::Tim4> { 
	static constexpr uint32_t Groups = (1ul << 0); static constexpr int8_t af = 0; };
template<> struct SignalConnection<DataB8::Rx, Peripheral::Can> { 
	static constexpr uint32_t Groups = (1ul << 2); static constexpr int8_t af = 2; };
template<> struct SignalConnection<DataB8::Scl, Peripheral::I2c1> { 
	static constexpr uint32_t Groups = (1ul << 1); static constexpr int8_t af = 1; };

struct DataB9 {
	static constexpr Gpio::Port port = Gpio::Port::B;
	static constexpr uint8_t pin = 9;
	struct BitBang { using Data = DataB9; static constexpr Gpio::Signal Signal = Gpio::Signal::BitBang; };
	struct Ch4 { using Data = DataB9; static constexpr Gpio::Signal Signal = Gpio::Signal::Ch4; };
	struct Sda { using Data = DataB9; static constexpr Gpio::Signal Signal = Gpio::Signal::Sda; };
	struct Tx { using Data = DataB9; static constexpr Gpio::Signal Signal = Gpio::Signal::Tx; };
};
template<Peripheral p> struct SignalConnection<DataB9::BitBang, p> {
	static_assert(p == Peripheral::BitBang, "GpioB9::BitBang only connects to software drivers!"); };
template<Peripheral p> struct SignalConnection<DataB9::Ch4, p> {
	static_assert((p == Peripheral::Tim4),"GpioB9::Ch4 only connects to Tim4!"); };
template<Peripheral p> struct SignalConnection<DataB9::Sda, p> {
	static_assert((p == Peripheral::I2c1),"GpioB9::Sda only connects to I2c1!"); };
template<Peripheral p> struct SignalConnection<DataB9::Tx, p> {
	static_assert((p == Peripheral::Can),"GpioB9::Tx only connects to Can!"); };
template<> struct SignalConnection<DataB9::BitBang, Peripheral::BitBang> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataB9::Ch4, Peripheral::Tim4> { 
	static constexpr uint32_t Groups = (1ul << 0); static constexpr int8_t af = 0; };
template<> struct SignalConnection<DataB9::Sda, Peripheral::I2c1> { 
	static constexpr uint32_t Groups = (1ul << 1); static constexpr int8_t af = 1; };
template<> struct SignalConnection<DataB9::Tx, Peripheral::Can> { 
	static constexpr uint32_t Groups = (1ul << 2); static constexpr int8_t af = 2; };

struct DataB10 {
	static constexpr Gpio::Port port = Gpio::Port::B;
	static constexpr uint8_t pin = 10;
	struct BitBang { using Data = DataB10; static constexpr Gpio::Signal Signal = Gpio::Signal::BitBang; };
	struct Ch3 { using Data = DataB10; static constexpr Gpio::Signal Signal = Gpio::Signal::Ch3; };
	struct Scl { using Data = DataB10; static constexpr Gpio::Signal Signal = Gpio::Signal::Scl; };
	struct Tx { using Data = DataB10; static constexpr Gpio::Signal Signal = Gpio::Signal::Tx; };
};
template<Peripheral p> struct SignalConnection<DataB10::BitBang, p> {
	static_assert(p == Peripheral::BitBang, "GpioB10::BitBang only connects to software drivers!"); };
template<Peripheral p> struct SignalConnection<DataB10::Ch3, p> {
	static_assert((p == Peripheral::Tim2),"GpioB10::Ch3 only connects to Tim2!"); };
template<Peripheral p> struct SignalConnection<DataB10::Scl, p> {
	static_assert((p == Peripheral::I2c2),"GpioB10::Scl only connects to I2c2!"); };
template<Peripheral p> struct SignalConnection<DataB10::Tx, p> {
	static_assert((p == Peripheral::Usart3),"GpioB10::Tx only connects to Usart3!"); };
template<> struct SignalConnection<DataB10::BitBang, Peripheral::BitBang> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataB10::Ch3, Peripheral::Tim2> { 
	static constexpr uint32_t Groups = (1ul << 2) | (1ul << 3); static constexpr int8_t af = 2; };
template<> struct SignalConnection<DataB10::Scl, Peripheral::I2c2> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataB10::Tx, Peripheral::Usart3> { 
	static constexpr uint32_t Groups = (1ul << 0); static constexpr int8_t af = 0; };

struct DataB11 {
	static constexpr Gpio::Port port = Gpio::Port::B;
	static constexpr uint8_t pin = 11;
	struct BitBang { using Data = DataB11; static constexpr Gpio::Signal Signal = Gpio::Signal::BitBang; };
	struct Ch4 { using Data = DataB11; static constexpr Gpio::Signal Signal = Gpio::Signal::Ch4; };
	struct Rx { using Data = DataB11; static constexpr Gpio::Signal Signal = Gpio::Signal::Rx; };
	struct Sda { using Data = DataB11; static constexpr Gpio::Signal Signal = Gpio::Signal::Sda; };
};
template<Peripheral p> struct SignalConnection<DataB11::BitBang, p> {
	static_assert(p == Peripheral::BitBang, "GpioB11::BitBang only connects to software drivers!"); };
template<Peripheral p> struct SignalConnection<DataB11::Ch4, p> {
	static_assert((p == Peripheral::Tim2),"GpioB11::Ch4 only connects to Tim2!"); };
template<Peripheral p> struct SignalConnection<DataB11::Rx, p> {
	static_assert((p == Peripheral::Usart3),"GpioB11::Rx only connects to Usart3!"); };
template<Peripheral p> struct SignalConnection<DataB11::Sda, p> {
	static_assert((p == Peripheral::I2c2),"GpioB11::Sda only connects to I2c2!"); };
template<> struct SignalConnection<DataB11::BitBang, Peripheral::BitBang> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataB11::Ch4, Peripheral::Tim2> { 
	static constexpr uint32_t Groups = (1ul << 2) | (1ul << 3); static constexpr int8_t af = 2; };
template<> struct SignalConnection<DataB11::Rx, Peripheral::Usart3> { 
	static constexpr uint32_t Groups = (1ul << 0); static constexpr int8_t af = 0; };
template<> struct SignalConnection<DataB11::Sda, Peripheral::I2c2> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };

struct DataB12 {
	static constexpr Gpio::Port port = Gpio::Port::B;
	static constexpr uint8_t pin = 12;
	struct BitBang { using Data = DataB12; static constexpr Gpio::Signal Signal = Gpio::Signal::BitBang; };
	struct Bkin { using Data = DataB12; static constexpr Gpio::Signal Signal = Gpio::Signal::Bkin; };
	struct Ck { using Data = DataB12; static constexpr Gpio::Signal Signal = Gpio::Signal::Ck; };
	struct Nss { using Data = DataB12; static constexpr Gpio::Signal Signal = Gpio::Signal::Nss; };
	struct Smba { using Data = DataB12; static constexpr Gpio::Signal Signal = Gpio::Signal::Smba; };
};
template<Peripheral p> struct SignalConnection<DataB12::BitBang, p> {
	static_assert(p == Peripheral::BitBang, "GpioB12::BitBang only connects to software drivers!"); };
template<Peripheral p> struct SignalConnection<DataB12::Bkin, p> {
	static_assert((p == Peripheral::Tim1),"GpioB12::Bkin only connects to Tim1!"); };
template<Peripheral p> struct SignalConnection<DataB12::Ck, p> {
	static_assert((p == Peripheral::Usart3),"GpioB12::Ck only connects to Usart3!"); };
template<Peripheral p> struct SignalConnection<DataB12::Nss, p> {
	static_assert((p == Peripheral::Spi2),"GpioB12::Nss only connects to Spi2!"); };
template<Peripheral p> struct SignalConnection<DataB12::Smba, p> {
	static_assert((p == Peripheral::I2c2),"GpioB12::Smba only connects to I2c2!"); };
template<> struct SignalConnection<DataB12::BitBang, Peripheral::BitBang> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataB12::Bkin, Peripheral::Tim1> { 
	static constexpr uint32_t Groups = (1ul << 0); static constexpr int8_t af = 0; };
template<> struct SignalConnection<DataB12::Ck, Peripheral::Usart3> { 
	static constexpr uint32_t Groups = (1ul << 0); static constexpr int8_t af = 0; };
template<> struct SignalConnection<DataB12::Nss, Peripheral::Spi2> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataB12::Smba, Peripheral::I2c2> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };

struct DataB13 {
	static constexpr Gpio::Port port = Gpio::Port::B;
	static constexpr uint8_t pin = 13;
	struct BitBang { using Data = DataB13; static constexpr Gpio::Signal Signal = Gpio::Signal::BitBang; };
	struct Ch1n { using Data = DataB13; static constexpr Gpio::Signal Signal = Gpio::Signal::Ch1n; };
	struct Cts { using Data = DataB13; static constexpr Gpio::Signal Signal = Gpio::Signal::Cts; };
	struct Sck { using Data = DataB13; static constexpr Gpio::Signal Signal = Gpio::Signal::Sck; };
};
template<Peripheral p> struct SignalConnection<DataB13::BitBang, p> {
	static_assert(p == Peripheral::BitBang, "GpioB13::BitBang only connects to software drivers!"); };
template<Peripheral p> struct SignalConnection<DataB13::Ch1n, p> {
	static_assert((p == Peripheral::Tim1),"GpioB13::Ch1n only connects to Tim1!"); };
template<Peripheral p> struct SignalConnection<DataB13::Cts, p> {
	static_assert((p == Peripheral::Usart3),"GpioB13::Cts only connects to Usart3!"); };
template<Peripheral p> struct SignalConnection<DataB13::Sck, p> {
	static_assert((p == Peripheral::Spi2),"GpioB13::Sck only connects to Spi2!"); };
template<> struct SignalConnection<DataB13::BitBang, Peripheral::BitBang> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataB13::Ch1n, Peripheral::Tim1> { 
	static constexpr uint32_t Groups = (1ul << 0); static constexpr int8_t af = 0; };
template<> struct SignalConnection<DataB13::Cts, Peripheral::Usart3> { 
	static constexpr uint32_t Groups = (1ul << 0) | (1ul << 1); static constexpr int8_t af = 0; };
template<> struct SignalConnection<DataB13::Sck, Peripheral::Spi2> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };

struct DataB14 {
	static constexpr Gpio::Port port = Gpio::Port::B;
	static constexpr uint8_t pin = 14;
	struct BitBang { using Data = DataB14; static constexpr Gpio::Signal Signal = Gpio::Signal::BitBang; };
	struct Ch2n { using Data = DataB14; static constexpr Gpio::Signal Signal = Gpio::Signal::Ch2n; };
	struct Miso { using Data = DataB14; static constexpr Gpio::Signal Signal = Gpio::Signal::Miso; };
	struct Rts { using Data = DataB14; static constexpr Gpio::Signal Signal = Gpio::Signal::Rts; };
};
template<Peripheral p> struct SignalConnection<DataB14::BitBang, p> {
	static_assert(p == Peripheral::BitBang, "GpioB14::BitBang only connects to software drivers!"); };
template<Peripheral p> struct SignalConnection<DataB14::Ch2n, p> {
	static_assert((p == Peripheral::Tim1),"GpioB14::Ch2n only connects to Tim1!"); };
template<Peripheral p> struct SignalConnection<DataB14::Miso, p> {
	static_assert((p == Peripheral::Spi2),"GpioB14::Miso only connects to Spi2!"); };
template<Peripheral p> struct SignalConnection<DataB14::Rts, p> {
	static_assert((p == Peripheral::Usart3),"GpioB14::Rts only connects to Usart3!"); };
template<> struct SignalConnection<DataB14::BitBang, Peripheral::BitBang> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataB14::Ch2n, Peripheral::Tim1> { 
	static constexpr uint32_t Groups = (1ul << 0); static constexpr int8_t af = 0; };
template<> struct SignalConnection<DataB14::Miso, Peripheral::Spi2> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataB14::Rts, Peripheral::Usart3> { 
	static constexpr uint32_t Groups = (1ul << 0) | (1ul << 1); static constexpr int8_t af = 0; };

struct DataB15 {
	static constexpr Gpio::Port port = Gpio::Port::B;
	static constexpr uint8_t pin = 15;
	struct BitBang { using Data = DataB15; static constexpr Gpio::Signal Signal = Gpio::Signal::BitBang; };
	struct Ch3n { using Data = DataB15; static constexpr Gpio::Signal Signal = Gpio::Signal::Ch3n; };
	struct Mosi { using Data = DataB15; static constexpr Gpio::Signal Signal = Gpio::Signal::Mosi; };
};
template<Peripheral p> struct SignalConnection<DataB15::BitBang, p> {
	static_assert(p == Peripheral::BitBang, "GpioB15::BitBang only connects to software drivers!"); };
template<Peripheral p> struct SignalConnection<DataB15::Ch3n, p> {
	static_assert((p == Peripheral::Tim1),"GpioB15::Ch3n only connects to Tim1!"); };
template<Peripheral p> struct SignalConnection<DataB15::Mosi, p> {
	static_assert((p == Peripheral::Spi2),"GpioB15::Mosi only connects to Spi2!"); };
template<> struct SignalConnection<DataB15::BitBang, Peripheral::BitBang> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataB15::Ch3n, Peripheral::Tim1> { 
	static constexpr uint32_t Groups = (1ul << 0); static constexpr int8_t af = 0; };
template<> struct SignalConnection<DataB15::Mosi, Peripheral::Spi2> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };

struct DataC13 {
	static constexpr Gpio::Port port = Gpio::Port::C;
	static constexpr uint8_t pin = 13;
	struct BitBang { using Data = DataC13; static constexpr Gpio::Signal Signal = Gpio::Signal::BitBang; };
	struct Out { using Data = DataC13; static constexpr Gpio::Signal Signal = Gpio::Signal::Out; };
	struct Tamper { using Data = DataC13; static constexpr Gpio::Signal Signal = Gpio::Signal::Tamper; };
};
template<Peripheral p> struct SignalConnection<DataC13::BitBang, p> {
	static_assert(p == Peripheral::BitBang, "GpioC13::BitBang only connects to software drivers!"); };
template<Peripheral p> struct SignalConnection<DataC13::Out, p> {
	static_assert((p == Peripheral::Rtc),"GpioC13::Out only connects to Rtc!"); };
template<Peripheral p> struct SignalConnection<DataC13::Tamper, p> {
	static_assert((p == Peripheral::Rtc),"GpioC13::Tamper only connects to Rtc!"); };
template<> struct SignalConnection<DataC13::BitBang, Peripheral::BitBang> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataC13::Out, Peripheral::Rtc> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataC13::Tamper, Peripheral::Rtc> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };

struct DataC14 {
	static constexpr Gpio::Port port = Gpio::Port::C;
	static constexpr uint8_t pin = 14;
	struct BitBang { using Data = DataC14; static constexpr Gpio::Signal Signal = Gpio::Signal::BitBang; };
	struct Osc32in { using Data = DataC14; static constexpr Gpio::Signal Signal = Gpio::Signal::Osc32in; };
};
template<Peripheral p> struct SignalConnection<DataC14::BitBang, p> {
	static_assert(p == Peripheral::BitBang, "GpioC14::BitBang only connects to software drivers!"); };
template<Peripheral p> struct SignalConnection<DataC14::Osc32in, p> {
	static_assert((p == Peripheral::Rcc),"GpioC14::Osc32in only connects to Rcc!"); };
template<> struct SignalConnection<DataC14::BitBang, Peripheral::BitBang> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataC14::Osc32in, Peripheral::Rcc> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };

struct DataC15 {
	static constexpr Gpio::Port port = Gpio::Port::C;
	static constexpr uint8_t pin = 15;
	struct BitBang { using Data = DataC15; static constexpr Gpio::Signal Signal = Gpio::Signal::BitBang; };
	struct Osc32out { using Data = DataC15; static constexpr Gpio::Signal Signal = Gpio::Signal::Osc32out; };
};
template<Peripheral p> struct SignalConnection<DataC15::BitBang, p> {
	static_assert(p == Peripheral::BitBang, "GpioC15::BitBang only connects to software drivers!"); };
template<Peripheral p> struct SignalConnection<DataC15::Osc32out, p> {
	static_assert((p == Peripheral::Rcc),"GpioC15::Osc32out only connects to Rcc!"); };
template<> struct SignalConnection<DataC15::BitBang, Peripheral::BitBang> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataC15::Osc32out, Peripheral::Rcc> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };

struct DataD0 {
	static constexpr Gpio::Port port = Gpio::Port::D;
	static constexpr uint8_t pin = 0;
	struct BitBang { using Data = DataD0; static constexpr Gpio::Signal Signal = Gpio::Signal::BitBang; };
	struct Oscin { using Data = DataD0; static constexpr Gpio::Signal Signal = Gpio::Signal::Oscin; };
	struct Rx { using Data = DataD0; static constexpr Gpio::Signal Signal = Gpio::Signal::Rx; };
};
template<Peripheral p> struct SignalConnection<DataD0::BitBang, p> {
	static_assert(p == Peripheral::BitBang, "GpioD0::BitBang only connects to software drivers!"); };
template<Peripheral p> struct SignalConnection<DataD0::Oscin, p> {
	static_assert((p == Peripheral::Rcc),"GpioD0::Oscin only connects to Rcc!"); };
template<Peripheral p> struct SignalConnection<DataD0::Rx, p> {
	static_assert((p == Peripheral::Can),"GpioD0::Rx only connects to Can!"); };
template<> struct SignalConnection<DataD0::BitBang, Peripheral::BitBang> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataD0::Oscin, Peripheral::Rcc> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataD0::Rx, Peripheral::Can> { 
	static constexpr uint32_t Groups = (1ul << 3); static constexpr int8_t af = 3; };

struct DataD1 {
	static constexpr Gpio::Port port = Gpio::Port::D;
	static constexpr uint8_t pin = 1;
	struct BitBang { using Data = DataD1; static constexpr Gpio::Signal Signal = Gpio::Signal::BitBang; };
	struct Oscout { using Data = DataD1; static constexpr Gpio::Signal Signal = Gpio::Signal::Oscout; };
	struct Tx { using Data = DataD1; static constexpr Gpio::Signal Signal = Gpio::Signal::Tx; };
};
template<Peripheral p> struct SignalConnection<DataD1::BitBang, p> {
	static_assert(p == Peripheral::BitBang, "GpioD1::BitBang only connects to software drivers!"); };
template<Peripheral p> struct SignalConnection<DataD1::Oscout, p> {
	static_assert((p == Peripheral::Rcc),"GpioD1::Oscout only connects to Rcc!"); };
template<Peripheral p> struct SignalConnection<DataD1::Tx, p> {
	static_assert((p == Peripheral::Can),"GpioD1::Tx only connects to Can!"); };
template<> struct SignalConnection<DataD1::BitBang, Peripheral::BitBang> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataD1::Oscout, Peripheral::Rcc> { 
	static constexpr uint32_t Groups = uint32_t(-1); static constexpr int8_t af = -1; };
template<> struct SignalConnection<DataD1::Tx, Peripheral::Can> { 
	static constexpr uint32_t Groups = (1ul << 3); static constexpr int8_t af = 3; };

} // namespace modm::platform::detail
/// @endcond