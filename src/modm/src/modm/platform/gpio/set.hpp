/*
 * Copyright (c) 2018, Niklas Hauser
 *
 * This file is part of the modm project.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
// ----------------------------------------------------------------------------

#pragma once

#include "../device.hpp"
#include "base.hpp"

namespace modm::platform
{

/// @ingroup modm_platform_gpio
template< class... Gpios >
class GpioSet : public Gpio
{
protected:
	static constexpr uint16_t inverteds[4] = {
		(((Gpios::port == Port::A and Gpios::isInverted) ? Gpios::mask : 0) | ...),
		(((Gpios::port == Port::B and Gpios::isInverted) ? Gpios::mask : 0) | ...),
		(((Gpios::port == Port::C and Gpios::isInverted) ? Gpios::mask : 0) | ...),
		(((Gpios::port == Port::D and Gpios::isInverted) ? Gpios::mask : 0) | ...),
	};
	static constexpr uint16_t inverted(uint8_t id) { return inverteds[id]; }

	static constexpr uint16_t masks[4] = {
		(((Gpios::port == Port::A) ? Gpios::mask : 0) | ...),
		(((Gpios::port == Port::B) ? Gpios::mask : 0) | ...),
		(((Gpios::port == Port::C) ? Gpios::mask : 0) | ...),
		(((Gpios::port == Port::D) ? Gpios::mask : 0) | ...),
	};
	static constexpr uint16_t mask(uint8_t id) { return masks[id]; }
	static constexpr uint32_t mask2(uint8_t id, uint8_t value = 0b11) {
		uint32_t r{0};
		for (int ii=0; ii<16; ii++)
			if (masks[id] & (1 << ii)) r |= (uint32_t(value) << (ii * 2));
		return r;
	}
	static constexpr uint64_t mask4(uint8_t id, uint8_t value = 0b1111) {
		uint64_t r{0};
		for (int ii=0; ii<16; ii++)
			if (masks[id] & (1 << ii)) r |= (uint64_t(value) << (ii * 4));
		return r;
	}
	static constexpr uint32_t crh(uint8_t id, uint8_t value = 0b1111) {
		return mask4(id, value) >> 32;
	}
	static constexpr uint32_t crl(uint8_t id, uint8_t value = 0b1111) {
		return mask4(id, value);
	}
	static constexpr uint8_t numberOfPorts() {
		uint8_t r{0};
		for (const auto &m: masks) r += (m) ? 1 : 0;
		return r;
	}

public:
	static constexpr uint8_t width = sizeof...(Gpios);
	static constexpr uint8_t number_of_ports = numberOfPorts();
public:
	static void setOutput()
	{
		configure(OutputType::PushPull);
	}

	static void setOutput(bool status)
	{
		set(status);
		setOutput();
	}

	static void setOutput(OutputType type, OutputSpeed speed = OutputSpeed::MHz50)
	{
		configure(type, speed);
	}

	static void configure(OutputType type, OutputSpeed speed = OutputSpeed::MHz50)
	{
		if constexpr (crl(0)) GPIOA->CRL = (GPIOA->CRL & ~crl(0)) | ((i(type) | i(speed)) * crl(0, 0b0001));
		if constexpr (crh(0)) GPIOA->CRH = (GPIOA->CRH & ~crh(0)) | ((i(type) | i(speed)) * crh(0, 0b0001));
		if constexpr (crl(1)) GPIOB->CRL = (GPIOB->CRL & ~crl(1)) | ((i(type) | i(speed)) * crl(1, 0b0001));
		if constexpr (crh(1)) GPIOB->CRH = (GPIOB->CRH & ~crh(1)) | ((i(type) | i(speed)) * crh(1, 0b0001));
		if constexpr (crl(2)) GPIOC->CRL = (GPIOC->CRL & ~crl(2)) | ((i(type) | i(speed)) * crl(2, 0b0001));
		if constexpr (crh(2)) GPIOC->CRH = (GPIOC->CRH & ~crh(2)) | ((i(type) | i(speed)) * crh(2, 0b0001));
		if constexpr (crl(3)) GPIOD->CRL = (GPIOD->CRL & ~crl(3)) | ((i(type) | i(speed)) * crl(3, 0b0001));
		if constexpr (crh(3)) GPIOD->CRH = (GPIOD->CRH & ~crh(3)) | ((i(type) | i(speed)) * crh(3, 0b0001));
	}

	static void setInput()
	{
		configure(InputType::Floating);
	}

	static void setInput(InputType type)
	{
		configure(type);
	}

	static void setAnalogInput()
	{
		if constexpr (crl(0)) GPIOA->CRL &= ~crl(0);
		if constexpr (crh(0)) GPIOA->CRH &= ~crh(0);
		if constexpr (crl(1)) GPIOB->CRL &= ~crl(1);
		if constexpr (crh(1)) GPIOB->CRH &= ~crh(1);
		if constexpr (crl(2)) GPIOC->CRL &= ~crl(2);
		if constexpr (crh(2)) GPIOC->CRH &= ~crh(2);
		if constexpr (crl(3)) GPIOD->CRL &= ~crl(3);
		if constexpr (crh(3)) GPIOD->CRH &= ~crh(3);
	}

	static void configure(InputType type)
	{
		if constexpr (crl(0)) GPIOA->CRL = (GPIOA->CRL & ~crl(0)) | ((i(type) & 0xc) * crl(0, 0b0001));
		if constexpr (crh(0)) GPIOA->CRH = (GPIOA->CRH & ~crh(0)) | ((i(type) & 0xc) * crh(0, 0b0001));
		if constexpr (mask(0)) GPIOA->BSRR = mask(0) << ((type == InputType::PullUp) ? 0 : 16);
		if constexpr (crl(1)) GPIOB->CRL = (GPIOB->CRL & ~crl(1)) | ((i(type) & 0xc) * crl(1, 0b0001));
		if constexpr (crh(1)) GPIOB->CRH = (GPIOB->CRH & ~crh(1)) | ((i(type) & 0xc) * crh(1, 0b0001));
		if constexpr (mask(1)) GPIOB->BSRR = mask(1) << ((type == InputType::PullUp) ? 0 : 16);
		if constexpr (crl(2)) GPIOC->CRL = (GPIOC->CRL & ~crl(2)) | ((i(type) & 0xc) * crl(2, 0b0001));
		if constexpr (crh(2)) GPIOC->CRH = (GPIOC->CRH & ~crh(2)) | ((i(type) & 0xc) * crh(2, 0b0001));
		if constexpr (mask(2)) GPIOC->BSRR = mask(2) << ((type == InputType::PullUp) ? 0 : 16);
		if constexpr (crl(3)) GPIOD->CRL = (GPIOD->CRL & ~crl(3)) | ((i(type) & 0xc) * crl(3, 0b0001));
		if constexpr (crh(3)) GPIOD->CRH = (GPIOD->CRH & ~crh(3)) | ((i(type) & 0xc) * crh(3, 0b0001));
		if constexpr (mask(3)) GPIOD->BSRR = mask(3) << ((type == InputType::PullUp) ? 0 : 16);
	}

	static void set()
	{
		if constexpr (mask(0)) GPIOA->BSRR = (inverted(0) << 16) | (mask(0) & ~inverted(0));
		if constexpr (mask(1)) GPIOB->BSRR = (inverted(1) << 16) | (mask(1) & ~inverted(1));
		if constexpr (mask(2)) GPIOC->BSRR = (inverted(2) << 16) | (mask(2) & ~inverted(2));
		if constexpr (mask(3)) GPIOD->BSRR = (inverted(3) << 16) | (mask(3) & ~inverted(3));
	}

	static void set(bool status)
	{
		if (status) set();
		else        reset();
	}

	static void reset()
	{
		if constexpr (mask(0)) GPIOA->BSRR = ((uint32_t(mask(0)) & ~inverted(0)) << 16) | inverted(0);
		if constexpr (mask(1)) GPIOB->BSRR = ((uint32_t(mask(1)) & ~inverted(1)) << 16) | inverted(1);
		if constexpr (mask(2)) GPIOC->BSRR = ((uint32_t(mask(2)) & ~inverted(2)) << 16) | inverted(2);
		if constexpr (mask(3)) GPIOD->BSRR = ((uint32_t(mask(3)) & ~inverted(3)) << 16) | inverted(3);
	}

	static void toggle()
	{
		if constexpr (mask(0)) {
			const uint32_t are_set = (GPIOA->ODR & mask(0));
			const uint32_t are_reset = mask(0) ^ are_set;
			GPIOA->BSRR = (are_set << 16) | are_reset;
		}
		if constexpr (mask(1)) {
			const uint32_t are_set = (GPIOB->ODR & mask(1));
			const uint32_t are_reset = mask(1) ^ are_set;
			GPIOB->BSRR = (are_set << 16) | are_reset;
		}
		if constexpr (mask(2)) {
			const uint32_t are_set = (GPIOC->ODR & mask(2));
			const uint32_t are_reset = mask(2) ^ are_set;
			GPIOC->BSRR = (are_set << 16) | are_reset;
		}
		if constexpr (mask(3)) {
			const uint32_t are_set = (GPIOD->ODR & mask(3));
			const uint32_t are_reset = mask(3) ^ are_set;
			GPIOD->BSRR = (are_set << 16) | are_reset;
		}
	}

	static void lock()
	{
		if constexpr (mask(0)) {
			GPIOA->LCKR = 0x10000 | mask(0);
			GPIOA->LCKR = 0x00000 | mask(0);
			GPIOA->LCKR = 0x10000 | mask(0);
			(void) GPIOA->LCKR;
		}
		if constexpr (mask(1)) {
			GPIOB->LCKR = 0x10000 | mask(1);
			GPIOB->LCKR = 0x00000 | mask(1);
			GPIOB->LCKR = 0x10000 | mask(1);
			(void) GPIOB->LCKR;
		}
		if constexpr (mask(2)) {
			GPIOC->LCKR = 0x10000 | mask(2);
			GPIOC->LCKR = 0x00000 | mask(2);
			GPIOC->LCKR = 0x10000 | mask(2);
			(void) GPIOC->LCKR;
		}
		if constexpr (mask(3)) {
			GPIOD->LCKR = 0x10000 | mask(3);
			GPIOD->LCKR = 0x00000 | mask(3);
			GPIOD->LCKR = 0x10000 | mask(3);
			(void) GPIOD->LCKR;
		}
	}

	static void disconnect()
	{
		(Gpios::disconnect(), ...);
	}
};

} // namespace modm::platform