/*
 * Copyright (c) 2016-2018, Niklas Hauser
 *
 * This file is part of the modm project.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
// ----------------------------------------------------------------------------

#ifndef MODM_STM32_GPIO_BASE_HPP
#define MODM_STM32_GPIO_BASE_HPP

#include "../device.hpp"
#include <modm/architecture/interface/gpio.hpp>
#include <modm/math/utils/bit_operation.hpp>
#include <modm/platform/core/peripherals.hpp>

namespace modm::platform
{

/// @ingroup modm_platform_gpio
struct Gpio
{
	enum class
	InputType
	{
		Floating = 0x4,	///< floating on input
		PullUp = 0x9,	///< pull-up on input
		PullDown = 0x8,	///< pull-down on input
	};

	enum class
	OutputType
	{
		PushPull = 0x0,		///< push-pull on output
		OpenDrain = 0x4,	///< open-drain on output
	};

	enum class
	OutputSpeed
	{
		Low    = 0x2,
		Medium = 0x1,
		High   = 0x3,
		MHz2   = Low,
		MHz10  = Medium,
		MHz50  = High,
	};

	/// The Port a Gpio Pin is connected to.
	enum class
	Port
	{
		A = 0,
		B = 1,
		C = 2,
		D = 3,
	};

	/// @cond
	enum class
	Signal
	{
		BitBang,
		Bkin,
		Ch1,
		Ch1n,
		Ch2,
		Ch2n,
		Ch3,
		Ch3n,
		Ch4,
		Ck,
		Cts,
		Dm,
		Dp,
		Etr,
		In0,
		In1,
		In2,
		In3,
		In4,
		In5,
		In6,
		In7,
		In8,
		In9,
		Jtck,
		Jtdi,
		Jtdo,
		Jtms,
		Mco,
		Miso,
		Mosi,
		Njtrst,
		Nss,
		Osc32in,
		Osc32out,
		Oscin,
		Oscout,
		Out,
		Rts,
		Rx,
		Sck,
		Scl,
		Sda,
		Smba,
		Swclk,
		Swdio,
		Tamper,
		Traceswo,
		Tx,
		Wkup,
	};
	/// @endcond

protected:
	/// @cond
	// Enum Class To Integer helper functions.
	static constexpr uint32_t
	i(InputType pull) { return uint32_t(pull); }
	static constexpr uint32_t
	i(OutputType out) { return uint32_t(out); }
	static constexpr uint32_t
	i(OutputSpeed speed) { return uint32_t(speed); }
	/// @endcond
};

} // namespace modm::platform

#endif // MODM_STM32_GPIO_BASE_HPP