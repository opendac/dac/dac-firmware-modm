/*
 * Copyright (c) 2019, Niklas Hauser
 *
 * This file is part of the modm project.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
// ----------------------------------------------------------------------------

#pragma once

namespace modm::platform
{

/// @ingroup modm_platform_core
enum class
Peripheral
{
	BitBang,
	Adc1,
	Adc2,
	Can,
	Crc,
	Dma1,
	Dma2,
	Flash,
	I2c1,
	I2c2,
	Iwdg,
	Rcc,
	Rtc,
	Spi1,
	Spi2,
	Sys,
	Tim1,
	Tim2,
	Tim3,
	Tim4,
	Usart1,
	Usart2,
	Usart3,
	Usb,
	Wwdg,
	Syscfg = Sys,
};

}