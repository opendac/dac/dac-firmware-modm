#include <modm/board.hpp>
#include <modm/platform.hpp>
#include <FreeRTOS.h>
#include <task.h>

void blinkTask(void* arguments)
{
    (void) arguments;
    while (1)
    {
        GpioC13::reset();
        vTaskDelay(pdMS_TO_TICKS(500));
        GpioC13::set();
        vTaskDelay(pdMS_TO_TICKS(500));
    }
}

TaskHandle_t xHandle = NULL;

int main()
{
    Board::initialize();
    GpioC13::setOutput();

    BaseType_t xReturned;
    xReturned = xTaskCreate(
        blinkTask,
        "Blink",
        64,
        nullptr,
        0,
        &xHandle
    );

    (void) xReturned;

//    UsbFs::connect<GpioA12::Dp, GpioA11::Dm>();
//    UsbFs::initialize<Board::SystemClock>();

     // Start the real time scheduler.
     vTaskStartScheduler();

    while (true)
    {
        GpioC13::toggle();
        modm::delay(100ms);
    }

    return 0;
}
